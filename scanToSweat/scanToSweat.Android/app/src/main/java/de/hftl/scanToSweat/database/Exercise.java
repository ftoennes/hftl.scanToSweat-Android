package de.hftl.scanToSweat.database;

/**
 * Created by lsbah on 29.11.2017.
 */
public class Exercise {

    private String id;
    private String description;
    private String exerciseName;
    private String machineName;
    private String imageUrl;
    private String muscleGroup;

    /**
     * Construtor for the Exercise.
     * @param String id
     * @param String description
     * @param String name
     * @param String imageUrl
     * @param String muscleGroup
     * @param String machineName
     */
    public Exercise(String id, String description, String name, String imageUrl, String muscleGroup, String machineName) {
        this.id = id;
        this.description = description;
        this.exerciseName = name;
        this.imageUrl = imageUrl;
        this.muscleGroup = muscleGroup;
        this.machineName = machineName;
    }

    /**
     * Getter for the Id.
     * @return String id
     */
    public String getId() {
        return id;
    }

    /**
     * Setter for the id.
     * @param String id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Getter for the Description.
     * @return String description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Setter for the Description.
     * @param String description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Getter for the ExerciseName.
     * @return String exerciseName
     */
    public String getExerciseName() {
        return exerciseName;
    }

    /**
     * Setter for the exerciseName.
     * @param String exerciseName
     */
    public void setExerciseName(String exerciseName) {
        this.exerciseName = exerciseName;
    }

    /**
     * Getter for the ImageUrl.
     * @return String ImageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * Setter for the ImageUrl.
     * @param String ImageUrl
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     * Getter for the MuscleGroup.
     * @return String MuscleGroup
     */
    public String getMuscleGroup() {
        return muscleGroup;
    }

    /**
     * Setter for the MuscleGroup.
     * @param String MuscleGroup
     */
    public void setMuscleGroup(String muscleGroup) {
        this.muscleGroup = muscleGroup;
    }

    /**
     * Getter for the MachineName.
     * @return String MachineName
     */
    public String getMachineName() {
        return machineName;
    }

    /**
     * Setter for the MachineName.
     * @param String MachineName
     */
    public void setMachineName(String machineName) {
        this.machineName = machineName;
    }
}
