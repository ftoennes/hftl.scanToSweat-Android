package de.hftl.scanToSweat.activities.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import de.hftl.scanToSweat.R;
import de.hftl.scanToSweat.activities.AllExercisesActivity;
import de.hftl.scanToSweat.activities.TrainingshistoryActivity;
import de.hftl.scanToSweat.database.DatabaseHelper;
import de.hftl.scanToSweat.database.Executed;
import de.hftl.scanToSweat.utils.Util;

/**
 * contains trainingsgraph
 * can call allexercises, trainingshistory
 *
 * @createdBy leoschul on 12.10.2017.
 * @editedBy KoPreuss
 */

public class TrainingFragment extends Fragment {

    Button buttonAllExercises;
    Button buttonTraningshistory;
    LineChart chart;

    /**
     * Construktor.
     * Does nothing
     */
    public TrainingFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_training, container, false);

        buttonAllExercises = ((Button) rootView.findViewById(android.R.id.button1));
        buttonAllExercises.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                Intent intent = new Intent(getActivity(), AllExercisesActivity.class);
                startActivity(intent);
            }
        });

        buttonTraningshistory = ((Button) rootView.findViewById(android.R.id.button2));
        buttonTraningshistory.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                Intent intent = new Intent(getActivity(), TrainingshistoryActivity.class);
                startActivity(intent);
            }
        });


        // add linechart
        chart = (LineChart) rootView.findViewById(R.id.chart);

        // generate chart
        generateChart();

        // chart styling
        styleChart(chart);

        // refresh chart
        chart.invalidate();

        // refresh chart
        chart.invalidate();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        updateGraph();
    }

    /**
     * Update graph.
     */
    public void updateGraph(){

        if (chart.getLineData() != null) {
            // clear chart view
            chart.clearValues();
            chart.clear();
        }

        // generate chart
        generateChart();

        // chart styling
        styleChart(chart);

        // update chart view
        chart.invalidate();
    }

    /**
     * Style graph.
     *
     * @param dataSet the data set
     */
    public void styleGraph(LineDataSet dataSet){
        // graph styling
        dataSet.setColor(getResources().getColor(R.color.lightgreen));
        dataSet.setLineWidth(5);
        dataSet.setValueTextColor(Color.TRANSPARENT);
        dataSet.setDrawCircles(true);
        dataSet.setCircleColor(getResources().getColor(R.color.lightgreen));
        dataSet.setCircleColorHole(getResources().getColor(R.color.lightgreen));
        dataSet.setCircleHoleRadius(0);
        dataSet.setCircleRadius(2.5f);
    }

    /**
     * Style chart.
     *
     * @param chart the chart
     */
    public void styleChart(LineChart chart){
        // chart styling
        chart.getDescription().setEnabled(true);
        chart.getDescription().setText(getLastScore() + "");
        chart.getDescription().setTextSize(24f);
        chart.setNoDataText(getResources().getString(R.string.training_chart_no_data));
        chart.setNoDataTextColor(getResources().getColor(R.color.grey));
        chart.getDescription().setPosition(350, 100);
        // no Legend
        chart.getLegend().setEnabled(false);
        // enable touch gestures
        chart.setTouchEnabled(true);
        chart.setDragDecelerationFrictionCoef(0.9f);
        // enable scaling and dragging
        chart.setDragEnabled(true);
        chart.setScaleEnabled(true);
        chart.setDrawGridBackground(false);
        chart.setHighlightPerDragEnabled(false);
        // enable borders
        chart.getXAxis().setEnabled(true);
        chart.getAxisLeft().setEnabled(true);
        chart.getAxisRight().setEnabled(true);
        // configure x Axis
        chart.getXAxis().setDrawGridLines(true);
        chart.getXAxis().setDrawLabels(false);
        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawAxisLine(true);
        xAxis.setAxisLineWidth(3);
        // configure y Axis
        chart.getAxisLeft().setDrawGridLines(false);
        chart.getAxisLeft().setDrawLabels(false);
        chart.getAxisLeft().setDrawAxisLine(true);
        chart.getAxisLeft().setAxisLineWidth(3);
        chart.getAxisRight().setDrawLabels(false);
        chart.getAxisRight().setDrawGridLines(false);
    }

    /**
     * Generate chart.
     */
    public void generateChart() {

        // for testing adding test values
//        int count = 3;
//        int range = 5;
//        ArrayList<Entry> entries = new ArrayList<Entry>();
//        for (int i = 0; i < count; i++) {
//
//            float val = (float) (Math.random() * range) + 3;
//            entries.add(new Entry(i, val));
//        }


        ArrayList<Entry> entries = new ArrayList<Entry>();
        LinkedHashMap<Date, Integer> scoresPerDay = getScorePerDay();

        if (scoresPerDay.size() > 0) {
            // add date and score to entryvalues of graph // baseentry
            int i = 1;
            entries.add(new Entry(0, 0));
            for (Map.Entry<Date, Integer> values : scoresPerDay.entrySet()) {
                entries.add(new Entry(i, values.getValue()));
                Log.i("entrie", values.getValue() + "");
                i++;
            }

            // add entries to dataset
            LineDataSet dataSet = new LineDataSet(entries, getResources().getString(R.string.training_chart_label));

            // graph styling
            styleGraph(dataSet);

            // add Data
            LineData lineData = new LineData(dataSet);
            chart.setData(lineData);
        }

    }

    /**
     * Calculates for every Day the Score and returns it as HashMap
     * @return HashMap<Date, Integer>
     */
    public LinkedHashMap<Date, Integer> getScorePerDay() {
        LinkedHashMap<Date, Integer> scores = new LinkedHashMap<>();

        DatabaseHelper dh = new DatabaseHelper(getContext());
        ArrayList<Executed> executes = dh.getAllExecutedExercises();

        boolean isFirst = true;

        Date currentDate = new Date(0);
        Double currentScore = 0.0;
        Integer counterGenerel = 0;
        for (Executed executed : executes) {
               boolean isSameDay = Util.isSameDay(currentDate, executed.getTime());
                if( executes.size() -1 == counterGenerel) {
                    Double initWeight = dh.getInitWeight(executed.getExerciseId(), executed.getWeight());
                    currentScore += executed.getRepeats() * (executed.getWeight() - initWeight + 1);
                    scores.put(currentDate, (int) Math.round(currentScore));
                    break;
                }
                if(isSameDay || isFirst) {
                    currentDate = executed.getTime();
                    Double initWeight = dh.getInitWeight(executed.getExerciseId(), executed.getWeight());
                    currentScore += executed.getRepeats() * (executed.getWeight() - initWeight + 1);
                    isFirst = false;
                } else {
                    scores.put(currentDate, (int) Math.round(currentScore));
                    currentScore = 0.0;
                    Double initWeight = dh.getInitWeight(executed.getExerciseId(), executed.getWeight());
                    currentScore += executed.getRepeats() * (executed.getWeight() - initWeight + 1);
                    currentDate = executed.getTime();
                }
            counterGenerel++;
        }
        return scores;
    }

    /**
     * Gets last score.
     *
     * @return the last score
     */
    public int getLastScore() {

        // if scores per day is empty, set last score to 0, else set to last score
        int lastScore = 0;

        // get score per day
        LinkedHashMap<Date, Integer> scoresPerDay = getScorePerDay();

        Log.i("lastscoreSize", scoresPerDay.size() + "");
        if (scoresPerDay.size() > 0) {
            for (int i = 0; i <= scoresPerDay.size() - 1; i++) {
                lastScore = scoresPerDay.values().iterator().next();

                Log.i("lastscore", lastScore + "");
            }
        }

        return lastScore;

    }

}
