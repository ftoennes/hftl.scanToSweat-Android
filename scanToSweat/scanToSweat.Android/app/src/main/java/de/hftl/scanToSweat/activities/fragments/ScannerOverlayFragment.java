package de.hftl.scanToSweat.activities.fragments;


import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.camera2.CameraManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import de.hftl.scanToSweat.R;
import de.hftl.scanToSweat.activities.fragments.ScannerFragment;
import de.hftl.scanToSweat.database.DatabaseHelper;
import de.hftl.scanToSweat.database.Exercise;
import de.hftl.scanToSweat.utils.SearchArrayAdapter;


/**
 * ScannerOverlayFragment
 */
public class ScannerOverlayFragment extends Fragment {


    public ScannerOverlayFragment() {
        // Required empty public constructor
    }
    public boolean isFlashLightOff = true;
    private Camera camera;
    View rootView;
    ScannerFragment scannerActivity;

    /**
     * Creating the View.
     * @param LayoutInflater inflater
     * @param ViewGroup container
     * @param Bundle savedInstanceState
     * @return View Rootview
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_scanner_overlay, container, false);

        scannerActivity =  (ScannerFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.scannerbackground);


        generateAutoComplete();

        final ImageButton imgButton = (ImageButton) rootView.findViewById(R.id.imgButton);
        imgButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                camera = scannerActivity.getCamera();
                if (getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)) {

                    if(isFlashLightOff) {
                        isFlashLightOff = false;
                        Camera.Parameters p = camera.getParameters();
                        p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                        camera.setParameters(p);
                        camera.startPreview();
                        imgButton.setImageResource(R.mipmap.ic_flash_on_white_24dp);
                    } else {
                        turnFlashlightOff(imgButton);
                    }
                }
            }
        });
        return rootView;
    }

    /**
     * Called after resume of the fragment.
     * Set's the Search Text to Empty
     * Get's the ScannerFragment new
     */
    @Override
    public void onResume() {
        scannerActivity =  (ScannerFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.scannerbackground);
        TextView autoText = (TextView) rootView.findViewById(R.id.autoText);
        autoText.setText("");
        super.onResume();
    }

    /**
     * Turn the Flashlight off and sets the ButtonImage new
     * @param imgButton
     */
    public void turnFlashlightOff(ImageButton imgButton) {
        isFlashLightOff = true;
        Camera.Parameters p = camera.getParameters();
        p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
        camera.setParameters(p);
        camera.startPreview();
        imgButton.setImageResource(R.mipmap.ic_flash_off_white_24dp);
    }

    /**
     * Generates for the Searchbar the Values to Search.
     */
    public void generateAutoComplete() {
        DatabaseHelper dh = new DatabaseHelper(getActivity());
        ArrayList<Exercise> exercises = dh.getAllExercises();
        List<String> text = new ArrayList<>();
        for (Exercise ex : exercises) {
            text.add(ex.getId() + " - " + ex.getExerciseName());
        }
        String[] values = text.toArray(new String[0]);


        ArrayAdapter<String> adapter = new SearchArrayAdapter(getContext(), R.layout.search_list, values);
        final AutoCompleteTextView textView = (AutoCompleteTextView) rootView.findViewById(R.id.autoText);
        textView.setAdapter(adapter);

    }

}
