package de.hftl.scanToSweat.activities;


import android.content.Context;
import android.hardware.Camera;
import android.hardware.input.InputManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;

import java.util.ArrayList;

import de.hftl.scanToSweat.R;
import de.hftl.scanToSweat.activities.fragments.ScannerFragment;
import de.hftl.scanToSweat.activities.fragments.ScannerOverlayFragment;
import de.hftl.scanToSweat.activities.fragments.SettingsFragment;
import de.hftl.scanToSweat.activities.fragments.TrainingFragment;
import de.hftl.scanToSweat.database.DatabaseHelper;
import de.hftl.scanToSweat.utils.ViewPagerAdapter;

public class StartActivity extends AppCompatActivity {

    private ViewPager viewPager;
    public ScannerFragment fragmentScanner;
    public TrainingFragment trainingFragment;
    public ScannerOverlayFragment overlayFragment;
    private Camera camera;
    private boolean pressedOnce;

    /**
     * Creating the View.
     * @param Bundle savedInstanceState
     * @return View Rootview
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.ScanToSweatStyle);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        fragmentScanner = (ScannerFragment) getSupportFragmentManager().findFragmentById(R.id.scannerbackground);
        fragmentScanner.resumeFragment();
        fragmentScanner.inPreview = true;


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                camera = fragmentScanner.getCamera();
                final ImageButton imgButton = (ImageButton) findViewById(R.id.imgButton);

                final ImageView circle1 = (ImageView) findViewById(R.id.circel);
                final ImageView circle2 = (ImageView) findViewById(R.id.circel2);
                final ImageView circle3 = (ImageView) findViewById(R.id.circel3);


                if (position == 0) {
                    if(!overlayFragment.isFlashLightOff) {
                        overlayFragment.turnFlashlightOff(imgButton);
                    }
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    AutoCompleteTextView textView = (AutoCompleteTextView) findViewById(R.id.autoText);
                    imm.hideSoftInputFromWindow(textView.getWindowToken(), 0);
                    stopCamera();
                    circle1.setVisibility(View.VISIBLE);
                    circle2.setVisibility(View.INVISIBLE);
                    circle3.setVisibility(View.INVISIBLE);
                } else if (position == 1) {
                    startCamera();
                    circle1.setVisibility(View.INVISIBLE);
                    circle2.setVisibility(View.VISIBLE);
                    circle3.setVisibility(View.INVISIBLE);
                } else if (position == 2) {
                    circle1.setVisibility(View.INVISIBLE);
                    circle2.setVisibility(View.INVISIBLE);
                    circle3.setVisibility(View.VISIBLE);

                    if(!overlayFragment.isFlashLightOff) {
                        overlayFragment.turnFlashlightOff(imgButton);
                    }

                    trainingFragment.updateGraph();
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    AutoCompleteTextView textView = (AutoCompleteTextView) findViewById(R.id.autoText);
                    imm.hideSoftInputFromWindow(textView.getWindowToken(), 0);

                    stopCamera();


                }
            }


            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        setupViewPager(viewPager);


        final ImageButton imgButton1 = (ImageButton) findViewById(R.id.imageView11);
        imgButton1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                viewPager.setCurrentItem(0);
            }
        });

        final ImageButton imgButton2 = (ImageButton) findViewById(R.id.imageView21);
        imgButton2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (viewPager.getCurrentItem() == 1) {
                    DatabaseHelper dh = new DatabaseHelper(getBaseContext());
                    String keyboard = dh.getSettingsValue("open_keyboard_by_middle_button");
                    if ("true".equals(keyboard)) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                        AutoCompleteTextView textView = (AutoCompleteTextView) findViewById(R.id.autoText);
                        imm.showSoftInput(textView, InputMethodManager.SHOW_IMPLICIT);
                    }
                } else {
                    viewPager.setCurrentItem(1);
                }
            }
        });

        final ImageButton imgButton3 = (ImageButton) findViewById(R.id.imageView31);
        imgButton3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                viewPager.setCurrentItem(2);
            }
        });
        final ImageView circle1 = (ImageView) findViewById(R.id.circel);
        circle1.setVisibility(View.INVISIBLE);

    }


    @Override
    /**
     * Is called when the Activity resumes.
     */
    protected void onResume() {
        super.onResume();
        fragmentScanner = (ScannerFragment) getSupportFragmentManager().findFragmentById(R.id.scannerbackground);
        camera = fragmentScanner.getCamera();
        fragmentScanner.resumeFragment();
        if (viewPager.getCurrentItem() != 1) {
            stopCamera();
        }
    }
    @Override
    /**
     * Is called wehen the Avtivity pause
     */
    protected void onPause() {
        super.onPause();
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        AutoCompleteTextView textView = (AutoCompleteTextView) findViewById(R.id.autoText);
        imm.hideSoftInputFromWindow(textView.getWindowToken(), 0);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == event.KEYCODE_BACK){
            if(!pressedOnce){
                pressedOnce = true;
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.startactivity_onkeydown), Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        pressedOnce = false;
                    }
                }, 3000);
            } else if (pressedOnce){
                pressedOnce = false;
                onBackPressed();
            }
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    /**
     * Setup the Viewpager -> Add Fragment to the Scrallview.
     * @param ViewPager viewPager
     */
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        overlayFragment = new ScannerOverlayFragment();
        trainingFragment = new TrainingFragment();

        adapter.addFragment(new SettingsFragment());
        adapter.addFragment(overlayFragment);
        adapter.addFragment(trainingFragment);

        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(1);
        findViewById(R.id.circel).setVisibility(View.VISIBLE);
    }

    /**
     * Starts Camera in external Thread
     */
    public void startCamera() {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                if (camera != null) {
                    camera.startPreview();
                }
            }
        });
    }

    /**
     * Stops Camera in external Thread
     */
    public void stopCamera() {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                if (camera != null) {
                    camera.stopPreview();
                }
            }
        });
    }

}


