package de.hftl.scanToSweat.server;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

import de.hftl.scanToSweat.R;
import de.hftl.scanToSweat.database.DatabaseHelper;
import de.hftl.scanToSweat.database.Exercise;
import de.hftl.scanToSweat.activities.AllExercisesActivity;
import de.hftl.scanToSweat.activities.ExerciseActivity;

/**
 * Created by kopreuss on 27.11.2017.
 */
public class ServerConnection {

    /**
     * The Context.
     */
    Context context;

    private ProgressDialog progressDialog;
    private Bitmap bitmap = null;

    String responseString;

    /**
     * Instantiates a new Server connection.
     *
     * @param context the context
     */
    public ServerConnection(Context context) {
        this.context = context;
    }

    /**
     * NOT READY TO USE
     * <p>
     * Download image.
     *
     * @param urlStr the url str
     */
    public void downloadImage(String urlStr) {
        //progressDialog = ProgressDialog.show(((ExerciseActivity) context), "", "Downloading Image from " + urlStr);
        final String url = urlStr;

        new Thread() {
            public void run() {
                InputStream in = null;

                Message msg = Message.obtain();
                msg.what = 1;

                try {
                    in = openHttpConnection(url);
                    bitmap = BitmapFactory.decodeStream(in);
                    Bundle b = new Bundle();
                    b.putParcelable("bitmap", bitmap);
                    msg.setData(b);
                    in.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                ExerciseByIdMessageHandler.sendMessage(msg);
            }
        }.start();
    }

    /**
     * Gets exercise data.
     *
     * @param urlStr       the url str
     * @param activityName the activity name
     */
    public void getExerciseData(final String urlStr, final String activityName) {
        progressDialog = ProgressDialog.show(context, "", R.string.progressdialoge_text + " " + urlStr);

        final String url = urlStr;
        final String name = activityName;

        new Thread() {
            public void run() {

                //generate message
                Message msg = Message.obtain();
                msg.what = 1;
                Bundle b = new Bundle();

                //read from inputstream save into data set
                b.putString("data", readFromInputStream(url));
                msg.setData(b);

                //give result to messageHandler
                if (activityName.contains("ExerciseActivity")) {
                    ExerciseByIdMessageHandler.sendMessage(msg);
                } else if (activityName.contains("AllExercisesActivity")) {
                    AllExercisesMessageHandler.sendMessage(msg);
                }

            }
        }.start();

    }

    /**
     * Database update.
     *
     * @param urlStr the url str
     */
    public void DatabaseUpdate(final String urlStr) {
        final String url = urlStr;

        new Thread() {
            public void run() {

                //generate message
                Message msg = Message.obtain();
                msg.what = 1;
                Bundle b = new Bundle();

                //read from inputstream save into data set
                b.putString("data", readFromInputStream(url));
                msg.setData(b);

                //give result to messageHandler
                DatabaseUpdateMessageHandler.sendMessage(msg);

            }
        }.start();

    }

    private String readFromInputStream(String urlStr) {

        final String url = urlStr;

        InputStream in = null;
        String data = "";
        try {
            in = openHttpConnection(url);
            if (in == null) {
                // do something - response is a failure (500 or 404)
                // either stop thread and close activity or repeat request
            }

            // read from Inputstream
            BufferedReader br1 = new BufferedReader(new InputStreamReader(in));
            data = br1.readLine();
            in.close();
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        return data;

    }

    private InputStream openHttpConnection(String urlStr) {
        InputStream in = null;

        try {
            trustEveryone();
            URL url = new URL(urlStr);
            URLConnection urlConn = url.openConnection();

            if (!(urlConn instanceof HttpURLConnection)) {
                throw new IOException("URL is not an Http URL");
            }

            HttpURLConnection httpConn = (HttpURLConnection) urlConn;
            trustEveryone();
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect();
            final int resCode = httpConn.getResponseCode();

            if (resCode == HttpURLConnection.HTTP_OK) {
                in = httpConn.getInputStream();

                //for debugging
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//
//                    //stuff that updates ui
//                    ((TextView) findViewById(R.id.textView4)).append("\nResCode:" + resCode);
//
//                }
//            });

                return in;

            } else {
                //return the responseCode if response is not OK_200 --> Request failed
                String stringResCode = "" + resCode;
                return new ByteArrayInputStream(stringResCode.getBytes(StandardCharsets.UTF_8.name()));
            }


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * The Exercise by idmessage handler.
     */
    public Handler ExerciseByIdMessageHandler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            final String response = msg.getData().getString("data");

            if (response.length() == 3) {

                ((Button) ((ExerciseActivity) context).findViewById(R.id.button2)).setVisibility(View.INVISIBLE);

                // Alert Dialog if any Erros occur
                ((ExerciseActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                        alertDialog.setTitle(context.getResources().getString(R.string.alertdialoge_title_responseerror) + " " + response);
                        alertDialog.setMessage(context.getResources().getString(R.string.alertdialoge_text_responseerror));
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, context.getResources().getString(R.string.alertdialoge_buttontext_responseerror),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();

                    }
                });
            } else {

                // doing stuff with response
                ((ExerciseActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        // response: [Übungsname];[Muskelgruppe];[ID für Zugehöriges Bild];[Gerätename];[Detaillierte Beschreibung]
                        // Übungsname;Muskelgruppe;ID für Zugehöriges Bild;Gerätename;Detaillierte Beschreibung
                        String[] responseArray;
                        responseArray = response.split(";");

                        // setExerciseDescriptionLong to description from response to re-use it incase user presses showmorebutton
                        ((ExerciseActivity) context).setExerciseDescriptionLong(responseArray[4]);

                        // fill textviews with response
                        ((TextView) ((ExerciseActivity) context).findViewById(R.id.textView2)).setText(responseArray[3]);
                        ((TextView) ((ExerciseActivity) context).findViewById(R.id.textView3)).setText(responseArray[0]);
                        ((TextView) ((ExerciseActivity) context).findViewById(R.id.textView4)).setText(responseArray[4]);
                        Log.i("handlerlonger2", "NOT inside if in handler" + ((TextView) ((ExerciseActivity) context).findViewById(R.id.textView4)).getLineCount());

                        // shorten description if longer than 2 lines
                        if (((TextView) ((ExerciseActivity) context).findViewById(R.id.textView4)).getLineCount() > 2) {
                            Log.i("handlerlonger2", "inside if in handler" + ((TextView) ((ExerciseActivity) context).findViewById(R.id.textView4)).getLineCount());
                            ((ExerciseActivity) context).setExerciseDescriptionLongLines(((TextView) ((ExerciseActivity) context).findViewById(R.id.textView4)).getLineCount());
                            ((TextView) ((ExerciseActivity) context).findViewById(R.id.textView4)).setLines(2);
                            ((Button) ((ExerciseActivity) context).findViewById(R.id.button1)).setClickable(true);
                            ((Button) ((ExerciseActivity) context).findViewById(R.id.button1)).setVisibility(View.VISIBLE);
                        }
                    }
                });

            }

            progressDialog.dismiss();
        }

    };

    /**
     * The All exercisesmessage handler.
     */
    public Handler AllExercisesMessageHandler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            final String response = msg.getData().getString("data");

            if (response.length() == 3) {

                // Alert Dialog if any Erros occur
                ((AllExercisesActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                        alertDialog.setTitle(context.getResources().getString(R.string.alertdialoge_title_responseerror) + " " + response);
                        alertDialog.setMessage(context.getResources().getString(R.string.alertdialoge_text_responseerror));
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, context.getResources().getString(R.string.alertdialoge_buttontext_responseerror),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();

                    }
                });
            } else {

                // doing stuff with response
                ((AllExercisesActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        // response:
                        // {"A05": "Bankdrücken Langhantel;Brust, Trizeps;Z_uJ;Bankdrücken;Diese Beschreibung folgt;","A13": "Negativ Bankdrücken;Brust, Trizeps;uyGM;Schrägbank;Diese Beschreibung folgt;","A01": "Brustpresse;Brust, Trizeps;qj8t;Brustpresse;Diese Beschreibung folgt;","A20": "Trizep Dips;Brust, Trizeps;G5to;Dipmaschine;Dies222 Beschreibung folgt;","A10": "Schrägbankdrücken Kurzhantel;Brust, Trizeps;twE3;Schrägbank;Diese Beschreibung folgt;"}

                        String[] responseArray;
                        responseArray = response.split("\",\"");

                        ((TextView) ((AllExercisesActivity) context).findViewById(R.id.textView1)).setText(response);

                    }
                });

            }

            progressDialog.dismiss();
        }
    };

    /**
     * The Database update message handler.
     */
    public Handler DatabaseUpdateMessageHandler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            // response is a String of data, except when something with serverconnection went wrong, than response is equal to errorcode (e.g. 404)
            final String response = msg.getData().getString("data");

            // if response is not as expected activate alert dialog
            if (response.length() == 3) {

                // Alert Dialog if any Erros occur
                // Toast.makeText(context, " Fehler bei Datenbank aktualisierung ", Toast.LENGTH_LONG).show();
                Log.i("databaseupdate", "Fehler bei Datenbank aktualisierung");

            } else {
                //Response wird auf die einzelenen Uebungen aufgesplited und in die loakle DB geschrieben
                //Wenn eine Uebung schon in der lokalen DB ist wird sie nicht hinzugefügt!
                String[] responses = response.split("\",\"");
                int i = 0;
                for (String resp : responses) {
                    resp = resp.replace("\"", "").replace("}", "").replace("{", "");

                    String[] execise = resp.split(":");
                    String[] execiseDetial = execise[1].split(";");
                    //response
                    // {"A05": "Bankdrücken Langhantel;Brust, Trizeps;Z_uJ;Bankdrücken;Diese Beschreibung folgt;",
                    String id = execise[0];
                    String ename = execiseDetial[0];
                    String gruppe = execiseDetial[1];
                    String bildUrl = execiseDetial[2];
                    String mname = execiseDetial[3];
                    String desc = execiseDetial[4];


                    DatabaseHelper dh = new DatabaseHelper(context);
                    Exercise ex = dh.getExerciseFromId(id);
                    if (ex == null) {
                        i++;
                        dh.insertExercise(id, ename, bildUrl, desc, gruppe, mname);
                    }
                }
                if(i > 0) {
                    Log.i("databaseupdate", "Update: Es wurden " + i + " Geräte der Datenbank hinzugefügt");
                    //Toast.makeText(context, "Update: Es wurden " + i + " Geräte der Datenbank hinzugefügt.", Toast.LENGTH_LONG).show();
                } else {
                    Log.i("databaseupdate", "Geräte Datenbank ist aktuell");
                    //Toast.makeText(context, "Geräte Datenbank ist aktuell!", Toast.LENGTH_LONG).show();
                }

            }
        }
    };

    /**
     * Check internet connection boolean.
     *
     * @return the boolean
     */
    public boolean checkInternetConnection() {
        // get Connectivity Manager object to check connection
        ConnectivityManager connec
                = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);

        // Check for network connections
        if (connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {
            Log.i("checkinternetconnection", "Connected");
            //Toast.makeText(context, " Connected ", Toast.LENGTH_LONG).show();
            return true;
        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED) {
            Log.i("checkinternetconnection", "Not Connected");
            //Toast.makeText(context, " Not Connected ", Toast.LENGTH_LONG).show();
            return false;
        }
        return false;
    }

    private void trustEveryone() {
        try {
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, new X509TrustManager[]{new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
            }}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
        } catch (Exception e) { // should never happen
            e.printStackTrace();
        }
    }

}
