package de.hftl.scanToSweat.utils;

import android.view.View;

/**
 * Created by lsbah on 06.12.2017.
 + Custom Listener to Give Variables into the Clickevnt
 */

public class CustomButtonListener implements View.OnClickListener
{

    String id;
    String name;
    int set;
    double weight;
    int repeats;

    /**
     * Construtor to Set Id and Name.
     * @param String id
     * @param String name
     */
    public CustomButtonListener(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public CustomButtonListener(String id, int set, double weight, int repeats){
        this.id = id;
        this.set = set;
        this.weight = weight;
        this.repeats = repeats;
    }

    public String getName() {
        return name;
    }

    /**
     * Getter for Id
     * @return String Id
     */
    public String getId() {
        return id;
    }

    public int getSet(){ return set; }

    public double getWeight() { return weight; }

    public int getRepeats() { return repeats; }

    @Override
    public void onClick(View v)
    {
        //read your lovely variable
    }
}
