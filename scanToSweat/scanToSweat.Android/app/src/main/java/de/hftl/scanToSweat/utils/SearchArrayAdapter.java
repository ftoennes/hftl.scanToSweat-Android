package de.hftl.scanToSweat.utils;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import de.hftl.scanToSweat.R;
import de.hftl.scanToSweat.activities.ExerciseActivity;
import de.hftl.scanToSweat.database.DatabaseHelper;

/**
 * Created by lsbah on 21.12.2017.
 */

public class SearchArrayAdapter extends ArrayAdapter<String> {
    private final Context context;
    private final String[] values;

    /**
     * Default Construtor
     * @param Context context
     * @param int textViewResourceId
     * @param String[] values
     */
    public SearchArrayAdapter(Context context, int textViewResourceId, String[] values) {
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;
    }

    @Override
    /**
     * Sets the texts into the Fields and add a Clicklistener to open ExecisePage.
     */
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.search_list, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.text1);
        TextView textView2 = (TextView) rowView.findViewById(R.id.text2);

        String[] names = this.getItem(position).split(" - ");

        Log.i("Search", "Pos: " + position);

        textView.setText(names[0]);
        textView2.setText(names[1]);
        // Change the icon for Windows and iPhone
        textView.setOnClickListener(new CustomButtonListener(names[0], names[1]) {
            public void onClick(View v) {
                Intent scannedLine = new Intent(getContext(), ExerciseActivity.class);
                scannedLine.putExtra("scannedLineFields", getId());
                getContext().startActivity(scannedLine);

            }
        });

        textView2.setOnClickListener(new CustomButtonListener(names[0], names[1]) {
            public void onClick(View v) {
                Intent scannedLine = new Intent(getContext(), ExerciseActivity.class);
                scannedLine.putExtra("scannedLineFields", getId());
                getContext().startActivity(scannedLine);

            }
        });



        return rowView;
    }

}

