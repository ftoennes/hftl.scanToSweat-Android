package de.hftl.scanToSweat.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.MotionEventCompat;
import android.transition.Fade;
import android.transition.Slide;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;

import de.hftl.scanToSweat.R;
import de.hftl.scanToSweat.database.DatabaseHelper;
import de.hftl.scanToSweat.database.Exercise;
import de.hftl.scanToSweat.server.ServerConnection;
import de.hftl.scanToSweat.utils.CustomButtonListener;


/**
 * shows all exercises
 *
 * @createdBy KoPreuss
 */
public class AllExercisesActivity extends FragmentActivity {

    @Override
    /**
     * Creating the View.
     * @param Bundle savedInstanceState
     * @return View Rootview
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_allexercises);

        ServerConnection sc = new ServerConnection(this);
        Drawable img = this.getResources().getDrawable(R.mipmap.ic_keyboard_arrow_right_black_24dp);
        DatabaseHelper dbh = new DatabaseHelper(this);

        ArrayList<Exercise> allExecirses = dbh.getAllExercises();
        for (Exercise exe : allExecirses) {
            Button btn = new Button(this);
             btn.setText(exe.getExerciseName());
            btn.setId(allExecirses.indexOf(exe));
            btn.setGravity(View.TEXT_ALIGNMENT_TEXT_START);
            btn.setTextSize(20);
            btn.setBackgroundColor(this.getResources().getColor(R.color.transparent));

            btn.setCompoundDrawablesWithIntrinsicBounds(null, null, img, null );
            LinearLayout layout = (LinearLayout) findViewById(R.id.linear_layout);
            layout.addView(btn);

            btn.setOnClickListener(new CustomButtonListener(exe.getId(), exe.getExerciseName()) {
                public void onClick(View view) {

                    Intent detail = new Intent(getBaseContext(), ExerciseDetailActivity.class);
                    detail.putExtra("ID", this.getId());
                    detail.putExtra("Name", this.getName());
                    startActivity(detail);


                }
            });
            View v = new View(this);
            v.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    1
            ));
            v.setBackgroundColor(this.getResources().getColor(R.color.background));
            layout.addView(v);

        }

    }

    float x1, x2;
    static final int MIN_DISTANCE = 250;

    @Override
    /**
     * Swipe Rigth to get Back to previos Page.
     */
    public boolean onTouchEvent(MotionEvent event){

        int action = MotionEventCompat.getActionMasked(event);
        switch(event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                x1 = event.getX();
                break;
            case MotionEvent.ACTION_UP:
                x2 = event.getX();
                float deltaX = x2 - x1;
                if (Math.abs(deltaX) > MIN_DISTANCE) {
                    this.onBackPressed();
                }
                break;
        }
        return super.onTouchEvent(event);
    }


}
