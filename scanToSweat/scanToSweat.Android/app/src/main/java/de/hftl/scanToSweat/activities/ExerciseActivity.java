package de.hftl.scanToSweat.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;

import de.hftl.scanToSweat.R;
import de.hftl.scanToSweat.database.DatabaseHelper;
import de.hftl.scanToSweat.database.Executed;
import de.hftl.scanToSweat.database.Exercise;
import de.hftl.scanToSweat.server.ServerConnection;
import de.hftl.scanToSweat.utils.SwipeLinearlayout;


/**
 * The type Exercise activity.
 *
 * @para Bundle savedInstanceState
 * @para Intent contains code from scanner <p> shows the current exercise using the scanned code
 * @createdBy KoPreuss
 */
public class ExerciseActivity extends FragmentActivity {

    ImageView imageViewPicture;
    TextView textViewExerciseId;
    TextView textViewDevicename;
    TextView textViewExercisename;
    TextView textViewExerciseDescriptionShort;
    Button buttonShowMore;
    Button buttonAddSet;

    LinearLayout linearLayoutVertical;

    boolean buttonShowMoreClicked = false;
    Date date = new Date();

    String exerciseDescriptionLong;
    int exerciseDescriptionLongLines;

    String exerciseId;

    private Bitmap bitmap = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise);

        ServerConnection sc = new ServerConnection(this);

        DatabaseHelper dbh = new DatabaseHelper(this);

        linearLayoutVertical = ((LinearLayout) this.findViewById(R.id.linear_layout_vertical));

        imageViewPicture = ((ImageView) this.findViewById(R.id.imageView1));
        textViewExerciseId = ((TextView) this.findViewById(R.id.textView1));
        textViewDevicename = ((TextView) this.findViewById(R.id.textView2));
        textViewExercisename = ((TextView) this.findViewById(R.id.textView3));
        textViewExerciseDescriptionShort = ((TextView) this.findViewById(R.id.textView4));
        buttonShowMore = ((Button) this.findViewById(R.id.button1));
        buttonAddSet = ((Button) this.findViewById(R.id.button2));


        imageViewPicture.setBackgroundColor(getResources().getColor(R.color.yellow));
        textViewExerciseId.setBackgroundColor(getResources().getColor(R.color.grey));
        textViewExerciseId.setTextColor(getResources().getColor(R.color.yellow));


        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (!bundle.isEmpty()) {
            exerciseId = (String) bundle.get("scannedLineFields");
            textViewExerciseId.setText(exerciseId);
        } else {
            textViewExerciseId.setText("Err");
        }

        buttonShowMore.setText(getResources().getString(R.string.exercise_buttonlabel_show_more));
        buttonShowMore.setBackgroundColor(Color.TRANSPARENT);
        buttonShowMore.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // if already clicked
                if (isButtonShowMoreClicked()) {
                    // Code here executes stuff after user presses button
                    textViewExerciseDescriptionShort.setLines(2);
                    buttonShowMore.setText(getResources().getString(R.string.exercise_buttonlabel_show_more));

                    setButtonShowMoreClicked(false);

                } else { // else if not clicked
                    // Code here executes stuff after user presses button
                    textViewExerciseDescriptionShort.setLines(getExerciseDescriptionLongLines());
                    textViewExerciseDescriptionShort.setText(getExerciseDescriptionLong());
                    buttonShowMore.setText(getResources().getString(R.string.exercise_buttonlabel_show_less));

                    setButtonShowMoreClicked(true);
                }

            }
        });

        Exercise ex = dbh.getExerciseFromId(exerciseId);
        // if exerciseId was found in locale database, use that dataset
        // else connect to server and get data from server database
        if (dbh.getExerciseFromId(exerciseId) != null) {
            textViewDevicename.setText(ex.getMachineName());
            textViewExercisename.setText(ex.getExerciseName());
            textViewExerciseDescriptionShort.setText(ex.getDescription());

            setExerciseDescriptionLong(ex.getDescription());

        } else {

            final String hostDomain = "https://lawnetix.org";
            final String betweenDomainAndPort = ":";
            final String port = "81";
            final String betweenPortAndId = "/api/exercise?id=";
            //[host-domain]:[Port]/api/exercise?id=[id]
            // https://lawnetix.org:81/api/exercise?id=A02
            String urlStr = hostDomain + betweenDomainAndPort + port + betweenPortAndId + exerciseId;

            sc.checkInternetConnection();
            sc.getExerciseData(urlStr, this.getLocalClassName());
        }

        // if there are already sets from current date (today) in the database, then show them in the verticallinearlayout
        if (!dbh.getSetsFromDay(date, exerciseId).isEmpty()) {
            // do something if there are sets from today in db

            // get Sets from day by date
            ArrayList<Executed> executs = dbh.getSetsFromDay(date, exerciseId);

            // iterated over each element in arraylsit and generate a set row in the verticallinearlayout
            for (Executed e : executs) {

                // add a grey spacer line above each row
                linearLayoutVertical.addView(generateSpacerLine());

                // generate row and add row to linearLayout
                linearLayoutVertical.addView(generateRowWithSwipe(e.getSet() + "", e.getRepeats() + "", e.getWeight() + ""));

            }

        }

        dbh.deleteAllFromToday(date, exerciseId);


        buttonAddSet.setText(getResources().getString(R.string.exercise_buttonlabel_add_set));
        buttonAddSet.setBackgroundColor(Color.WHITE);
        buttonAddSet.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // add a set on click

                // add a grey spacer line above each row
                linearLayoutVertical.addView(generateSpacerLine());

                // generate row and add row to linearLayout
                linearLayoutVertical.addView(generateRowWithSwipe(getChildCountFromLinearLayoutVertical() + "", "15", "100.0"));
                // RepeatsText and WeightText are default "15"


            }
        });


        // detect if keyboard is visible
        // if keyboard is visible, set button add set to gone
        // if keyboard is invisible, set button add set to visible
        ExerciseActivity.this.findViewById(R.id.ExcerciseRootLayout).getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                Rect r = new Rect();
                ExerciseActivity.this.findViewById(R.id.ExcerciseRootLayout).getWindowVisibleDisplayFrame(r);
                int screenHeight = ExerciseActivity.this.findViewById(R.id.ExcerciseRootLayout).getRootView().getHeight();

                // r.bottom is the position above soft keypad or device button.
                // if keypad is shown, the r.bottom is smaller than that before.
                int keypadHeight = screenHeight - r.bottom;

                Log.i("keypadheight", "keypadHeight = " + keypadHeight);

                if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                    // if keyboard is opened
                    ExerciseActivity.this.findViewById(R.id.button2).setVisibility(View.INVISIBLE);
                } else {
                    // if keyboard is closed
                    ExerciseActivity.this.findViewById(R.id.button2).setVisibility(View.VISIBLE);
                }
            }
        });

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        checkDescriptionLength();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        DatabaseHelper dbh = new DatabaseHelper(this);

        dbh.deleteAllFromToday(date, exerciseId);

        for (int i = 1; i < linearLayoutVertical.getChildCount(); i++) {
            Log.i("insertSet", linearLayoutVertical.getChildCount() + "");

            //multiple sets to save
            if (linearLayoutVertical.getChildCount() > 2 && (i % 2) != 0) {

                Integer set = Integer.parseInt(((TextView) ((LinearLayout) linearLayoutVertical.getChildAt(i)).getChildAt(0)).getText() + "");
                Integer repeats = Integer.parseInt(((TextView) ((LinearLayout) linearLayoutVertical.getChildAt(i)).getChildAt(2)).getText() + "");
                Double weight = Double.parseDouble(((TextView) ((LinearLayout) linearLayoutVertical.getChildAt(i)).getChildAt(4)).getText() + "");

                Log.i("insertSet", date + " " + exerciseId + " " + set + " " + weight + " " + repeats);

                dbh.insertExecutedExercise(date, exerciseId, set, weight, repeats);

            } else if (linearLayoutVertical.getChildCount() == 2) {
                // one set to save
                Integer set = Integer.parseInt(((TextView) ((LinearLayout) linearLayoutVertical.getChildAt(i)).getChildAt(0)).getText() + "");
                Integer repeats = Integer.parseInt(((TextView) ((LinearLayout) linearLayoutVertical.getChildAt(i)).getChildAt(2)).getText() + "");
                Double weight = Double.parseDouble(((TextView) ((LinearLayout) linearLayoutVertical.getChildAt(i)).getChildAt(4)).getText() + "");

                Log.i("insertSet", "one to save " + date + " " + exerciseId + " " + set + " " + weight + " " + repeats);

                dbh.insertExecutedExercise(date, exerciseId, set, weight, repeats);

            } else {
                // nothing to save or spacer

                Log.i("insertSet", "nothing to save or spacer");
            }
        }
    }

    /**
     * Check description length, if longer than 2 lines, than aktivate button show more
     */
    public void checkDescriptionLength() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.i("line", "" + textViewExerciseDescriptionShort.getLineCount());

                // shorten description if longer than 2 lines
                if (textViewExerciseDescriptionShort.getLineCount() > 2) {

                    // for testing
                    textViewExerciseDescriptionShort.append("longer 2");

                    //save the long version of description and the used lines
                    //setExerciseDescriptionLong(ex.getDescription());
                    setExerciseDescriptionLongLines(textViewExerciseDescriptionShort.getLineCount());
                    // limit description lines to 2 and set showmorebutton visible and clickable
                    textViewExerciseDescriptionShort.setLines(2);
                    buttonShowMore.setClickable(true);
                    buttonShowMore.setVisibility(View.VISIBLE);
                } else {
                    buttonShowMore.setClickable(false);
                    buttonShowMore.setVisibility(View.INVISIBLE);
                }
            }

        });
    }

    /**
     * Gets child count from linear layout vertical.
     *
     * @return the child count from linear layout vertical
     */
    public int getChildCountFromLinearLayoutVertical() {

        // get childcount
        int childcount;
        if (linearLayoutVertical.getChildCount() > 1) {
            childcount = linearLayoutVertical.getChildCount() - (linearLayoutVertical.getChildCount() / 2);
        } else {
            childcount = 1;
        }

        return childcount;
    }


    /**
     * Generate row with swipe swipe linearlayout.
     *
     * @param setNumberText the set number text
     * @param RepeatsText   the repeats text
     * @param WeightText    the weight text
     * @return the swipe linearlayout
     */
    public SwipeLinearlayout generateRowWithSwipe(String setNumberText, String RepeatsText, String WeightText) {

        // initialize rows linearLayout
        final SwipeLinearlayout swipeLayout = new SwipeLinearlayout(ExerciseActivity.this);

        // initialize elements in row
        final TextView textViewSetNumber = generateTextViewSetNumber(setNumberText);
        final Button buttonPlus = generateButtonPlus();
        final EditText editTextRepeats = generateEditTextRepeats(RepeatsText);
        final Button buttonMinus = generateButtonMinus();
        final EditText editTextWeight = generateEditTextWeight(WeightText);

        // add and configurate buttonlistner on button plus
        buttonPlus.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // do stuff on click

                // if current repeats are smaller than Integer Max value, increase by 1
                if (!editTextRepeats.getText().toString().equals(Integer.MAX_VALUE)) {
                    editTextRepeats.setText((Integer.parseInt(editTextRepeats.getText().toString()) + 1) + "");
                }

            }
        });

        // add and configurate buttonlistner on button minus
        buttonMinus.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // do stuff on click

                // if current repeats are greater than 0, reduce by 1
                if (!editTextRepeats.getText().toString().equals("0")) {
                    editTextRepeats.setText((Integer.parseInt(editTextRepeats.getText().toString()) - 1) + "");
                }

            }
        });

        // style all elements in row
        //swipeLayout.setWeightSum(3f);

        // set on swipe listener to swipelayout
        swipeLayout.setOnSwipeListener(new SwipeLinearlayout.OnSwipeListener() {
            @Override
            public void onSwipeLeft() {

                // get index of swipelayout in linearlayoutvertical view
                int index = ((ViewGroup) swipeLayout.getParent()).indexOfChild(swipeLayout);

                // remove swipelayout row with index i from linearlayoutvertical and remove the spacerline over the removed row
                linearLayoutVertical.removeViewAt(index);
                linearLayoutVertical.removeViewAt(index - 1);

                // update setnumbers
                updateSetNumbers();

                // update view
                linearLayoutVertical.invalidate();

            }
        });

        // add elements to row
        swipeLayout.addView(textViewSetNumber);
        swipeLayout.addView(buttonPlus);
        swipeLayout.addView(editTextRepeats);
        swipeLayout.addView(buttonMinus);
        swipeLayout.addView(editTextWeight);

        return swipeLayout;
    }

    /**
     * @param setNumberText the set number text
     * @param RepeatsText   the repeats text
     * @param WeightText    the weight text
     * @return the linear layout
     * @deprecated use {@link #generateRowWithSwipe(String, String, String)} ()} instead.
     * Generate row linear layout.
     */
    public LinearLayout generateRow(String setNumberText, String RepeatsText, String WeightText) {

        // initialize rows linearLayout
        LinearLayout linearLayout = new LinearLayout(ExerciseActivity.this);

        // initialize elements in row
        final TextView textViewSetNumber = generateTextViewSetNumber(setNumberText);
        final Button buttonPlus = generateButtonPlus();
        final EditText editTextRepeats = generateEditTextRepeats(RepeatsText);
        final Button buttonMinus = generateButtonMinus();
        final EditText editTextWeight = generateEditTextWeight(WeightText);

        // add and configurate buttonlistner on button plus
        buttonPlus.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // do stuff on click

                // if current repeats are smaller than Integer Max value, increase by 1
                if (!editTextRepeats.getText().toString().equals(Integer.MAX_VALUE)) {
                    editTextRepeats.setText((Integer.parseInt(editTextRepeats.getText().toString()) + 1) + "");
                }

            }
        });

        // add and configurate buttonlistner on button minus
        buttonMinus.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // do stuff on click

                // if current repeats are greater than 0, reduce by 1
                if (!editTextRepeats.getText().toString().equals("0")) {
                    editTextRepeats.setText((Integer.parseInt(editTextRepeats.getText().toString()) - 1) + "");
                }

            }
        });

        // style all elements in row
        linearLayout.setWeightSum(3f);

        // add elements to row
        linearLayout.addView(textViewSetNumber);
        linearLayout.addView(buttonPlus);
        linearLayout.addView(editTextRepeats);
        linearLayout.addView(buttonMinus);
        linearLayout.addView(editTextWeight);

        return linearLayout;
    }


    /**
     * Generate spacer line view - grey bar between set rows.
     *
     * @return the view
     */
    public View generateSpacerLine() {

        // add a grey above each row
        View viewSpacerLine = new View(ExerciseActivity.this);
        viewSpacerLine.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 1));
        viewSpacerLine.setBackgroundColor(ExerciseActivity.this.getResources().getColor(R.color.background));

        return viewSpacerLine;
    }

    /**
     * Generate text view set number text view.
     *
     * @param text the text
     * @return the text view
     */
    public TextView generateTextViewSetNumber(String text) {

        final TextView textViewSetNumber = new TextView(ExerciseActivity.this);

        // style textView SetNumber
        textViewSetNumber.setText(text + "");
        textViewSetNumber.setTextSize(18);
        textViewSetNumber.setTextColor(getResources().getColor(R.color.grey));
        textViewSetNumber.setGravity(Gravity.CENTER);
        textViewSetNumber.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 0.5f));

        return textViewSetNumber;
    }

    /**
     * Generate button plus button.
     *
     * @return the button
     */
    public Button generateButtonPlus() {

        final Button buttonPlus = new Button(ExerciseActivity.this);

        // style button plus
        buttonPlus.setText("+");
        buttonPlus.setTextSize(24);
        buttonPlus.setTextColor(getResources().getColor(R.color.grey));
        buttonPlus.setBackgroundColor(Color.TRANSPARENT);
        buttonPlus.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 0.4f)); // this three together 1.25

        return buttonPlus;
    }

    /**
     * Generate edit text repeats edit text.
     *
     * @param text the text
     * @return the edit text
     */
    public EditText generateEditTextRepeats(String text) {

        final EditText editTextRepeats = new EditText(ExerciseActivity.this);

        // style editText Repeats
        editTextRepeats.setText(text);
        editTextRepeats.setTextColor(getResources().getColor(R.color.grey));
        editTextRepeats.setGravity(Gravity.CENTER);
        editTextRepeats.setBackground(getResources().getDrawable(R.drawable.roundededittext));
        editTextRepeats.setBackgroundColor(Color.TRANSPARENT);
        editTextRepeats.setSingleLine(true);
        editTextRepeats.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
        editTextRepeats.setKeyListener(new DigitsKeyListener());
        editTextRepeats.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 0.45f));

        return editTextRepeats;
    }

    /**
     * Generate button minus button.
     *
     * @return the button
     */
    public Button generateButtonMinus() {

        final Button buttonMinus = new Button(ExerciseActivity.this);

        // style button minus
        buttonMinus.setText("-");
        buttonMinus.setTextSize(24);
        buttonMinus.setTextColor(getResources().getColor(R.color.grey));
        buttonMinus.setBackgroundColor(Color.TRANSPARENT);
        buttonMinus.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 0.4f));

        return buttonMinus;
    }

    /**
     * Generate edit text weight edit text.
     *
     * @param text the text
     * @return the edit text
     */
    public EditText generateEditTextWeight(String text) {

        final EditText editTextWeight = new EditText(ExerciseActivity.this);

        // style editText Weight
        editTextWeight.setText(text); // default 15 "100"
        editTextWeight.setTextColor(getResources().getColor(R.color.grey));
        editTextWeight.setGravity(Gravity.CENTER);
        editTextWeight.setBackground(getResources().getDrawable(R.drawable.roundededittext));
        editTextWeight.setBackgroundColor(Color.TRANSPARENT);
        editTextWeight.setSingleLine(true);
        editTextWeight.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
        editTextWeight.setKeyListener(new DigitsKeyListener(false,true));
        editTextWeight.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1.25f));

        return editTextWeight;
    }

    /**
     * Update set numbers.
     */
    public void updateSetNumbers() {

        for (int i = 1; i <= getChildCountFromLinearLayoutVertical(); i++) {
//            ((TextView) ((LinearLayout) linearLayoutVertical.getChildAt(i)).getChildAt(0)).setText(i + "");

//            ViewGroup swipeLayout = (ViewGroup) linearLayoutVertical.getChildAt(i);
//            TextView textView = (TextView) swipeLayout.getChildAt(1);
//            textView.setText(i + "");

//            String test = ((TextView) ((LinearLayout) linearLayoutVertical.getChildAt(i)).getChildAt(0)).getText() + "";
//            Toast.makeText(ExerciseActivity.this, test + "", Toast.LENGTH_LONG).show();


        }

        linearLayoutVertical.invalidate();


    }

    /**
     * Is button show more clicked boolean.
     *
     * @return the boolean
     */
    public boolean isButtonShowMoreClicked() {
        return buttonShowMoreClicked;
    }

    /**
     * Sets button show more clicked.
     *
     * @param buttonShowMoreClicked the button show more clicked
     */
    public void setButtonShowMoreClicked(boolean buttonShowMoreClicked) {
        this.buttonShowMoreClicked = buttonShowMoreClicked;
    }

    /**
     * Gets exercise description long lines.
     *
     * @return the exercise description long lines
     */
    public int getExerciseDescriptionLongLines() {
        return exerciseDescriptionLongLines;
    }

    /**
     * Sets exercise description long lines.
     *
     * @param exerciseDescriptionLongLines the exercise description long lines
     */
    public void setExerciseDescriptionLongLines(int exerciseDescriptionLongLines) {
        this.exerciseDescriptionLongLines = exerciseDescriptionLongLines;
    }

    /**
     * Gets string exercise description long.
     *
     * @return the string exercise description long
     */
    public String getExerciseDescriptionLong() {
        return exerciseDescriptionLong;
    }

    /**
     * Sets string exercise description long.
     *
     * @param exerciseDescriptionLong the string exercise description long
     */
    public void setExerciseDescriptionLong(String exerciseDescriptionLong) {
        this.exerciseDescriptionLong = exerciseDescriptionLong;
    }

}

