package de.hftl.scanToSweat.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import de.hftl.scanToSweat.R;
import de.hftl.scanToSweat.database.DatabaseHelper;
import de.hftl.scanToSweat.database.Executed;
import de.hftl.scanToSweat.database.Exercise;
import de.hftl.scanToSweat.utils.Util;

/**
 * contains the trainingshistory
 *
 * @createdBy KoPreuss
 */
public class TrainingshistoryActivity extends Activity {

ExpandableListAdapter expandableListAdapter;
ExpandableListView expandableListView;
List<String> listDataHeader;
HashMap<String, List<String>> listDataChild;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trainingshistory);

        ((TextView) this.findViewById(R.id.text1)).setText(getResources().getString(R.string.trainingshistory_titellabel));

        //expandableListTest
        //get the listview
        expandableListView = (ExpandableListView) findViewById(R.id.lvExp);

        //preparing List data
        prepareListData();

        expandableListAdapter = new de.hftl.scanToSweat.utils.ExpandableListAdapter(this, listDataHeader, listDataChild);
        //setting list adapter
        expandableListView.setAdapter(expandableListAdapter);

    }

    /*
    Preparing the list data
     */
    private void prepareListData(){

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        //list of Dates without repeats, has to be filled!
        listDataHeader = new ArrayList();


        //list with values for 1 date, has to be filled!
        listDataChild = new HashMap<String, List<String>>();
        DatabaseHelper databaseHelper = new DatabaseHelper(this);
        //DatabaseHelper helper = new DatabaseHelper(this);
        ArrayList<String> detailsforExecuted = new ArrayList<String>();

        //all Entries of Executed
        ArrayList<Executed> allExecuted = databaseHelper.getAllExecutedExercises();

        //only Dates of all entries
        ArrayList allDates = new ArrayList();
        //only Entries for one single Date
        ArrayList<Executed> entriesForOneDate = new ArrayList();
        //entries for one single date and one single node on that date
        ArrayList<Executed> entriesForOneDateAndOneNodeId = new ArrayList();
        //highest Set at one date on one Node
        Executed highestSetForOneDateAtOneNode;
        //ArrayList<Exercise> allexercises = helper.getAllExercises();

        String name = "nonExisting";
        ArrayList allExecutedNodeIds = new ArrayList();


        Date temp = new Date(2000000000);

        if (allExecuted==null){
            /*ArrayList<String> listForNull = new ArrayList<>();
            listForNull.add(name);
            listDataHeader.add(simpleDateFormat.format(temp));
            listDataChild.put(listDataHeader.get(0), listForNull);*/
        }
        else{
            for (int i = 0;  i < allExecuted.size(); i++){
                if(!Util.isSameDay(allExecuted.get(i).getTime(), temp)){
                    allDates.add(allExecuted.get(i).getTime());
                }
                temp = allExecuted.get(i).getTime();
            }

            for (int j = 0; j< allDates.size(); j++){
                listDataHeader.add(simpleDateFormat.format(allDates.get(j)));
            }

            for (int k = 0; k < allDates.size(); k++){
                entriesForOneDate = Util.filterChildListForGivenDate(allExecuted, (Date)allDates.get(k));
                allExecutedNodeIds = Util.filterAllExecutedForUniqueNodeId(entriesForOneDate);
                for (int h = 0; h < allExecutedNodeIds.size(); h++){
                    entriesForOneDateAndOneNodeId = Util.filterChildListForGivenExerciseID(entriesForOneDate, (String) allExecutedNodeIds.get(h));
                    highestSetForOneDateAtOneNode = Util.filterChildForHighestSet(entriesForOneDateAndOneNodeId);
                    String exerciseId = highestSetForOneDateAtOneNode.getExerciseId();
                    Exercise exercise = databaseHelper.getExerciseFromId(exerciseId);
                    if(exercise != null){
                        name = exercise.getExerciseName();
                    }
                    //detailsforExecuted.add("DEBUG: " + exerciise.getExerciseName());
                    detailsforExecuted.add(name + "\t" + "\t " + getResources().getString(R.string.trainingshistory_label_sets) + ": " + String.valueOf(highestSetForOneDateAtOneNode.getSet()) );
                    //detailsforExecuted.add(String.valueOf(highestSetForOneDateAtOneNode.getSet()) + " Sets");

                }

                listDataChild.put(listDataHeader.get(k), detailsforExecuted);
                detailsforExecuted = new ArrayList<>();
            }
        }

    }

    float x1, x2;
    static final int MIN_DISTANCE = 150;

    @Override
    public boolean onTouchEvent(MotionEvent event){

        int action = MotionEventCompat.getActionMasked(event);
        switch(event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                x1 = event.getX();
                break;
            case MotionEvent.ACTION_UP:
                x2 = event.getX();
                float deltaX = x2 - x1;
                if (Math.abs(deltaX) > MIN_DISTANCE) {
                    this.onBackPressed();
                }
                break;
        }
        return super.onTouchEvent(event);
    }
}
