package de.hftl.scanToSweat.activities.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.ImageFormat;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.abbyy.mobile.rtr.Engine;
import com.abbyy.mobile.rtr.IDataCaptureProfileBuilder;
import com.abbyy.mobile.rtr.IDataCaptureService;
import com.abbyy.mobile.rtr.Language;

import java.util.ArrayList;
import java.util.List;

import de.hftl.scanToSweat.BuildConfig;
import de.hftl.scanToSweat.R;
import de.hftl.scanToSweat.activities.ExerciseActivity;
import de.hftl.scanToSweat.server.ServerConnection;
/* Created by leoschul on 12.10.2017.*/

public class ScannerFragment extends Fragment {
    // Licensing
    private static final String licenseFileName = "AbbyyRtrSdk.license";

    ///////////////////////////////////////////////////////////////////////////////
    // Some application settings that can be changed to modify application behavior:
    // The camera zoom. Optically zooming with a good camera often improves results
    // even at close range and it might be required at longer ranges.
    private static final int cameraZoom = 1;
    // The default behavior in this sample is to start recognition when application is started or
    // resumed. You can turn off this behavior or remove it completely to simplify the application
    private static final boolean startRecognitionOnAppStart = true;
    // Area of interest specified through margin sizes relative to camera preview size
    private static final int areaOfInterestMargin_PercentOfWidth = 4;
    private static final int areaOfInterestMargin_PercentOfHeight = 25;

    // A subset of available languages shown in the UI. See all available languages in Language enum.
    // To show all languages in the UI you can substitute the list below with:
    // Language[] languages = Language.values();
    // A set of available sample data capture scenarios;
    private enum SampleDataCaptureScenarios {
        // Simple data capture scenarios with regular expressions
        McFit("Es wird nach möglichen McFit Nummern gesucht: X00"),
        FitnessFirst("Es wird nach möglichen FitnessFirst Nummern gesucht: 000");

        private SampleDataCaptureScenarios(String usage) {
            Usage = usage;
        }

        public String Usage;
    }

    private SampleDataCaptureScenarios currentScenario;
    ///////////////////////////////////////////////////////////////////////////////

    // The 'Abbyy RTR SDK Engine' and 'Text Capture Service' to be used in this sample application
    private Engine engine;
    private IDataCaptureService dataCaptureService;

    // The camera and the preview surface
    public Camera camera;
    public SurfaceViewWithOverlay surfaceViewWithOverlay;
    private SurfaceHolder previewSurfaceHolder;

    // Actual preview size
    private Camera.Size cameraPreviewSize;

    // Auxiliary variables
    public boolean inPreview = false; // Camera preview is started
    private boolean stableResultHasBeenReached; // Stable result has been reached
    private boolean startRecognitionWhenReady; // Start recognition next time when ready (and reset this flag)
    private Handler handler = new Handler(); // Posting some delayed actions;


    private Spinner dataCaptureSampleSpinner; // Data capture scenario selection
    // To communicate with the Text Capture Service we will need this callback:
    private IDataCaptureService.Callback dataCaptureCallback = new IDataCaptureService.Callback() {

        @Override
        public void onRequestLatestFrame(byte[] buffer) {
            // The service asks to fill the buffer with image data for the latest frame in NV21 format.
            // Delegate this task to the camera. When the buffer is filled we will receive
            // Camera.PreviewCallback.onPreviewFrame (see below)
            camera.addCallbackBuffer(buffer);
        }

        @Override
        public void onFrameProcessed(IDataCaptureService.DataScheme scheme, IDataCaptureService.DataField[] fields,
                                     IDataCaptureService.ResultStabilityStatus resultStatus, IDataCaptureService.Warning warning) {
            // Frame has been processed. Here we process recognition results. In this sample we
            // stop when we get stable result. This callback may continue being called for some time
            // even after the service has been stopped while the calls queued to this thread (UI thread)
            // are being processed. Just ignore these calls:
            if (!stableResultHasBeenReached) {
                if (scheme != null && resultStatus == IDataCaptureService.ResultStabilityStatus.Stable) {
                    // Stable result has been reached. Stop the service
                    stopRecognition();
                    stableResultHasBeenReached = true;

                    // start activity ExerciseActivity with scanned line fields as parameter
                    Intent scannedLine = new Intent(getActivity(), ExerciseActivity.class);
                    scannedLine.putExtra("scannedLineFields", fields[0].Text);
                    startActivity(scannedLine);

                }
            }
        }

        @Override
        public void onError(Exception e) {
            // An error occurred while processing. Log it. Processing will continue
            Log.e(getString(R.string.app_name), "Error: " + e.getMessage());
        }
    };


    // This callback will be used to obtain frames from the camera
    private Camera.PreviewCallback cameraPreviewCallback = new Camera.PreviewCallback() {
        @Override
        public void onPreviewFrame(byte[] data, Camera camera) {
            // The buffer that we have given to the camera in IDataCaptureService.Callback.onRequestLatestFrame
            // above have been filled. Send it back to the Data Capture Service
            dataCaptureService.submitRequestedFrame(data);
        }
    };
    // This callback is used to configure preview surface for the camera
    SurfaceHolder.Callback surfaceCallback = new SurfaceHolder.Callback() {

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            // When surface is created, store the holder
            previewSurfaceHolder = holder;
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            // When surface is changed (or created), attach it to the camera, configure camera and start preview
            if (camera != null) {
                setCameraPreviewDisplayAndStartPreview();
            }
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            // When surface is destroyed, clear previewSurfaceHolder
            previewSurfaceHolder = null;
        }
    };

    // Start recognition when autofocus completes (used when continuous autofocus is not enabled)
    private Camera.AutoFocusCallback startRecognitionCameraAutoFocusCallback = new Camera.AutoFocusCallback() {
        @Override
        public void onAutoFocus(boolean success, Camera camera) {
            onAutoFocusFinished(success, camera);
            startRecognition();
        }
    };

    // Simple autofocus callback
    private Camera.AutoFocusCallback simpleCameraAutoFocusCallback = new Camera.AutoFocusCallback() {
        @Override
        public void onAutoFocus(boolean success, Camera camera) {
            onAutoFocusFinished(success, camera);
        }
    };

    // Enable 'Start' button and switching to continuous focus mode (if possible) when autofocus complet
    private Camera.AutoFocusCallback finishCameraInitialisationAutoFocusCallback = new Camera.AutoFocusCallback() {
        @Override
        public void onAutoFocus(boolean success, Camera camera) {
            onAutoFocusFinished(success, camera);
            if (startRecognitionWhenReady) {
                startRecognition();
                startRecognitionWhenReady = false;
            }
        }
    };


    private void onAutoFocusFinished(boolean success, Camera camera) {
        if (isContinuousVideoFocusModeEnabled(camera)) {
            setCameraFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
        } else {
            if (!success) {
                autoFocus(simpleCameraAutoFocusCallback);
            }
        }
    }

    // Start autofocus (used when continuous autofocus is disabled)
    private void autoFocus(Camera.AutoFocusCallback callback) {
        if (camera != null) {
            try {
                setCameraFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                camera.autoFocus(callback);
            } catch (Exception e) {
                Log.e(getString(R.string.app_name), "Error: " + e.getMessage());
            }
        }
    }

    // Checks that FOCUS_MODE_CONTINUOUS_VIDEO supported
    private boolean isContinuousVideoFocusModeEnabled(Camera camera) {
        return camera.getParameters().getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
    }

    // Sets camera focus mode and focus area
    private void setCameraFocusMode(String mode) {
        // Camera sees it as rotated 90 degrees, so there's some confusion with what is width and what is height)
        int halfCoordinates = 1000;
        int lengthCoordinates = 2000;
        Rect area = surfaceViewWithOverlay.getAreaOfInterest();

        int width = cameraPreviewSize.height;
        int height = cameraPreviewSize.width;


        camera.cancelAutoFocus();
        Camera.Parameters parameters = camera.getParameters();
        // Set focus and metering area equal to the area of interest. This action is essential because by defaults camera
        // focuses on the center of the frame, while the area of interest in this sample application is at the top
        List<Camera.Area> focusAreas = new ArrayList<>();
        Rect areasRect = new Rect(
                        -halfCoordinates + area.top * lengthCoordinates / height,
                        halfCoordinates - area.right * lengthCoordinates / width,
                        -halfCoordinates + lengthCoordinates * area.bottom / height,
                        halfCoordinates - lengthCoordinates * area.left / width
                );


        focusAreas.add(new Camera.Area(areasRect, 800));
        parameters.setFocusAreas(focusAreas);
        parameters.setMeteringAreas(focusAreas);

        parameters.setFocusMode(mode);

        // Commit the camera parameters
        camera.setParameters(parameters);
    }

    // Attach the camera to the surface holder, configure the camera and start preview
    private void setCameraPreviewDisplayAndStartPreview() {
        try {
            camera.setPreviewDisplay(previewSurfaceHolder);
        } catch (Throwable t) {
            Log.e(getString(R.string.app_name), "Exception in setPreviewDisplay()", t);
        }
        configureCameraAndStartPreview(camera);
    }

    // Stop preview and release the camera
    private void stopPreviewAndReleaseCamera() {
        if (camera != null) {
            camera.setPreviewCallbackWithBuffer(null);
            if (inPreview) {
                camera.stopPreview();
                inPreview = false;
            }

            camera = null;
        }
    }

    // Show error on startup if any
    private void showStartupError(String message) {
        new AlertDialog.Builder(getActivity())
                .setTitle("ABBYY RTR SDK")
                .setMessage(message)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show()
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        getActivity().finish();
                    }
                });
    }

    // Create and configure data capture service
    private IDataCaptureService createConfigureDataCaptureService(SampleDataCaptureScenarios scenario) {
        // Custom data capture scenarios
        IDataCaptureService dataCaptureService = engine.createDataCaptureService(null, dataCaptureCallback);
        IDataCaptureProfileBuilder profileBuilder = dataCaptureService.configureDataCaptureProfile();
        IDataCaptureProfileBuilder.IFieldBuilder field = profileBuilder.addScheme(scenario.name()).addField(scenario.name());
        switch (scenario) {
            // Regular expression scenarios
            case McFit:
                profileBuilder.setRecognitionLanguage(Language.English);
                // z.B. A01
                field.setRegEx("[A-H][0-9]{2,}");
                break;
            case FitnessFirst:
                profileBuilder.setRecognitionLanguage(Language.English);
                //z.B. 021
                field.setRegEx("[0-9][0-9][0-9]");
                break;
            default:
                throw new IndexOutOfBoundsException();
        }
        profileBuilder.checkAndApply();
        return dataCaptureService;

    }

    // Load ABBYY RTR SDK engine and configure the data capture service
    private boolean createConfigureDataCaptureService() {
        // Initialize the engine and data capture service
        try {
            engine = Engine.load(getActivity(), licenseFileName);

            SampleDataCaptureScenarios selected = SampleDataCaptureScenarios.valueOf((String) dataCaptureSampleSpinner.getSelectedItem());
            dataCaptureService = createConfigureDataCaptureService(selected);
            currentScenario = selected;

            return true;
        } catch (java.io.IOException e) {
            // Troubleshooting for the developer
            Log.e(getString(R.string.app_name), "Error loading ABBYY RTR SDK:", e);
            showStartupError("Could not load some required resource files. Make sure to configure " +
                    "'assets' directory in your application and specify correct 'license file name'. See logcat for details.");
        } catch (Engine.LicenseException e) {
            // Troubleshooting for the developer
            Log.e(getString(R.string.app_name), "Error loading ABBYY RTR SDK:", e);
            showStartupError("License not valid. Make sure you have a valid license file in the " +
                    "'assets' directory and specify correct 'license file name' and 'application id'. See logcat for details.");
        } catch (Throwable e) {
            // Troubleshooting for the developer
            Log.e(getString(R.string.app_name), "Error loading ABBYY RTR SDK:", e);
            showStartupError("Unspecified error while loading the engine. See logcat for details.");
        }

        return false;
    }

    // Start recognition
    public void startRecognition() {
        // Do not switch off the screen while data capture service is running
        previewSurfaceHolder.setKeepScreenOn(true);

        // To select new scenario it is essential to create new DataCaptureService
        SampleDataCaptureScenarios selected = SampleDataCaptureScenarios.valueOf((String) dataCaptureSampleSpinner.getSelectedItem());
        if (selected != currentScenario) {
            dataCaptureService = createConfigureDataCaptureService(selected);
            currentScenario = selected;
            configureCameraAndStartPreview(camera);
        }

        // Get area of interest (in coordinates of preview frames)
        Rect areaOfInterest = new Rect(surfaceViewWithOverlay.getAreaOfInterest());
        // Start the service
        dataCaptureService.start(cameraPreviewSize.width, cameraPreviewSize.height, 90, areaOfInterest);
    }

    // Stop recognition
    public void stopRecognition() {

        // Stop the service asynchronously to make application more responsive. Stopping can take some time
        // waiting for all processing threads to stop
        new AsyncTask<Void, Void, Void>() {
            protected Void doInBackground(Void... params) {
                dataCaptureService.stop();
                return null;
            }

            protected void onPostExecute(Void result) {
                if (previewSurfaceHolder != null) {
                    // Restore normal power saving behaviour
                    previewSurfaceHolder.setKeepScreenOn(false);
                }
            }
        }.execute();
    }

    // Clear recognition results
    void clearRecognitionResults() {
        stableResultHasBeenReached = false;
    }

    private void configureCameraAndStartPreview(Camera camera) {
        camera.setDisplayOrientation(90);

        // Configure camera parameters
        Camera.Parameters parameters = camera.getParameters();

        // Select preview size. The preferred size for Text Capture scenario is 1080x720. In some scenarios you might
        // consider using higher resolution (small text, complex background) or lower resolution (better performance, less noise)
        cameraPreviewSize = null;
        for (Camera.Size size : parameters.getSupportedPreviewSizes()) {
            if (size.height <= 720 || size.width <= 720) {
                if (cameraPreviewSize == null) {
                    cameraPreviewSize = size;
                } else {
                    int resultArea = cameraPreviewSize.width * cameraPreviewSize.height;
                    int newArea = size.width * size.height;
                    if (newArea > resultArea) {
                        cameraPreviewSize = size;
                    }
                }
            }
        }
        parameters.setPreviewSize(cameraPreviewSize.width, cameraPreviewSize.height);

        // Zoom
        parameters.setZoom(cameraZoom);
        // Buffer format. The only currently supported format is NV21
        parameters.setPreviewFormat(ImageFormat.NV21);
        // Default focus mode
        parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);

        // Done
        camera.setParameters(parameters);

        // The camera will fill the buffers with image data and notify us through the callback.
        // The buffers will be sent to camera on requests from recognition service (see implementation
        // of ITextCaptureService.Callback.onRequestLatestFrame above)
        camera.setPreviewCallbackWithBuffer(cameraPreviewCallback);

        // Clear the previous recognition results if any
        clearRecognitionResults();

        // Width and height of the preview according to the current screen rotation
        int width = cameraPreviewSize.height;
        int height = cameraPreviewSize.width;


        // Area of interest
        int marginWidth = (areaOfInterestMargin_PercentOfWidth * width) / 100;
        int marginHeight = (areaOfInterestMargin_PercentOfHeight * height) / 100;
        surfaceViewWithOverlay.setAreaOfInterest(
                new Rect(marginWidth, marginHeight, width - marginWidth,
                        height - marginHeight));

        camera.startPreview();



        setCameraFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        autoFocus(finishCameraInitialisationAutoFocusCallback);

        inPreview = true;
    }


    // Initialize recognition language spinner in the UI with available languages
    private void initializeDataCaptureScenariosSpinner() {
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        // Make the collapsed spinner the size of the selected item
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, R.id.spinner_item_text) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
                view.setLayoutParams(params);

                return setScenarioUsage(view, position);
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                return setScenarioUsage(super.getDropDownView(position, convertView, parent), position);
            }

            View setScenarioUsage(View view, int position) {
                SampleDataCaptureScenarios scenario = SampleDataCaptureScenarios.values()[position];
                TextView usage = (TextView) view.findViewById(R.id.spinner_item_usage);
                usage.setText(scenario.Usage);
                return view;
            }
        };

        // Stored preference
        final String dataCaptureScenarioKey = "SampleDataCaptureScenario";
        String selectedSample = preferences.getString(dataCaptureScenarioKey, SampleDataCaptureScenarios.values()[0].toString());

        // Fill the spinner with available languages selecting the previously chosen language
        int selectedIndex = -1;
        for (int i = 0; i < SampleDataCaptureScenarios.values().length; i++) {
            SampleDataCaptureScenarios scenario = SampleDataCaptureScenarios.values()[i];
            String name = scenario.toString();
            adapter.add(name);
            if (name.equalsIgnoreCase(selectedSample)) {
                selectedIndex = i;
            }
        }
        if (selectedIndex == -1) {
            selectedIndex = 0;
        }

        dataCaptureSampleSpinner.setAdapter(adapter);

        if (selectedIndex != -1) {
            dataCaptureSampleSpinner.setSelection(selectedIndex);
        }

        // The callback to be called when a language is selected
        dataCaptureSampleSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String scanario = (String) parent.getItemAtPosition(position);
                if (!preferences.getString(dataCaptureScenarioKey, "").equalsIgnoreCase(scanario)) {
                    stopRecognition();
                    // Store the selection in preferences
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(dataCaptureScenarioKey, scanario);
                    editor.commit();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public ScannerFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_scanner, container, false);

        // Retrieve some ui components
        dataCaptureSampleSpinner = (Spinner) rootView.findViewById(R.id.dataCaptureSampleSpinner);

        // Initialize the recognition language spinner
        initializeDataCaptureScenariosSpinner();

        // Manually create preview surface. The only reason for this is to
        // avoid making it public top level class
        RelativeLayout layout = (RelativeLayout) dataCaptureSampleSpinner.getParent();

        surfaceViewWithOverlay = new SurfaceViewWithOverlay(getActivity());
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT);
        surfaceViewWithOverlay.setLayoutParams(params);
        // Add the surface to the layout as the bottom-most view filling the parent
        layout.addView(surfaceViewWithOverlay, 0);

        // Create text capture service
        if (createConfigureDataCaptureService()) {
            // Set the callback to be called when the preview surface is ready.
            // We specify it as the last step as a safeguard so that if there are problems
            // loading the engine the preview will never start and we will never attempt calling the service
            surfaceViewWithOverlay.getHolder().addCallback(surfaceCallback);
        }

        ServerConnection sc = new ServerConnection(getActivity());
        // check for Internet connection
        if (sc.checkInternetConnection()) {

            //[host-domain]:[Port]/api/all_exercises
            // https://lawnetix.org:81/api/all_exercises
            final String hostDomain = "https://lawnetix.org";
            final String port = ":81";
            final String apiMethode = "/api/all_exercises";

            // build url String
            String urlStr = hostDomain + port + apiMethode;

            // execute database update
            sc.DatabaseUpdate(urlStr);
        }


        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void resumeFragment() {
        clearRecognitionResults();
        startRecognitionWhenReady = startRecognitionOnAppStart;
        camera = Camera.open();
        if (previewSurfaceHolder != null) {
            setCameraPreviewDisplayAndStartPreview();
        }
    }

    @Override
    public void onPause() {
        // Clear all pending actions
        handler.removeCallbacksAndMessages(null);
        // Stop the text capture service
        if (dataCaptureService != null) {
            dataCaptureService.stop();
        }
        // Clear recognition results
        clearRecognitionResults();
        stopPreviewAndReleaseCamera();
        super.onPause();
    }

    // Surface View combined with an overlay showing recognition results and 'progress'
    static class SurfaceViewWithOverlay extends SurfaceView {
        private Rect areaOfInterest;

        public SurfaceViewWithOverlay(Context context) {
            super(context);
            this.setWillNotDraw(false);
        }

        public void setAreaOfInterest(Rect newValue) {
            areaOfInterest = newValue;
            invalidate();
        }

        public Rect getAreaOfInterest() {
            return areaOfInterest;
        }
    }

    public Camera getCamera() {
        return camera;
    }
}

