package de.hftl.scanToSweat.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import de.hftl.scanToSweat.utils.Util;

/**
 * Created by leoschul on 24.11.2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "scanToSweat.db";
    private static final int DATABASE_VERSION = 16;

    public static final String EXECUTED_TABLE_NAME = "ExecutedExercises";
    public static final String EXECUTED_COLUMN_TIME = "time";
    public static final String EXECUTED_COLUMN_EXERCISE_ID = "exerciseId";
    public static final String EXECUTED_COLUMN_SET = "set_id";
    public static final String EXECUTED_COLUMN_WEIGHT = "weight";
    public static final String EXECUTED_COLUMN_REPEATS = "repeats";
    public static final String EXECUTED_ID_PRIMARY_KEY = "id";

    public static final String EXERCISES_TABLE_NAME = "Exercises";
    public static final String EXERCISES_COLUMN_EXERCISE_ID = "exerciseId";
    public static final String EXERCISES_COLUMN_ENAME = "execiseName";
    public static final String EXERCISES_COLUMN_MNAME = "machineName";
    public static final String EXERCISES_COLUMN_DESCRIPTION = "description";
    public static final String EXERCISES_COLUMN_IMAGE_URL = "imageUrl";
    public static final String EXERCISES_COLUMN_MUSCLE_GROUP = "muscleGroup";

    public static final String SETTINGS_TABLE_NAME = "Settings";
    public static final String SETTINGS_COLUMN_ID = "settingsId";
    public static final String SETTINGS_COLUMN_VALUE= "value";

    public static final String INITWEIGHT_TABLE_NAME = "initWeight";
    public static final String INITWEIGHT_COLUMN_ID = "exerciseId";
    public static final String INITWEIGHT_COLUMN_WEIGHT= "weight";


    private Context context;

    /**
     * Default Construktor
     * @param Context context
     */
    public DatabaseHelper(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    /**
     * Erstellt die benötigten Datenbanken
     *
     * @param SQLiteDatabase db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS " + EXECUTED_TABLE_NAME +
                        "(" + EXECUTED_COLUMN_TIME + " TEXT, " +
                        EXECUTED_COLUMN_EXERCISE_ID + " TEXT, " +
                        EXECUTED_COLUMN_SET + " INTEGER, " +
                        EXECUTED_COLUMN_WEIGHT + " REAL, " +
                        EXECUTED_COLUMN_REPEATS + " INTEGER, " +
                        EXECUTED_ID_PRIMARY_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT)"
        );
        db.execSQL(
                "CREATE TABLE " + EXERCISES_TABLE_NAME +
                        "(" + EXERCISES_COLUMN_EXERCISE_ID + " TEXT PRIMARY KEY, " +
                        EXERCISES_COLUMN_ENAME + " TEXT, " +
                        EXERCISES_COLUMN_IMAGE_URL + " TEXT, " +
                        EXERCISES_COLUMN_DESCRIPTION + " TEXT, " +
                        EXERCISES_COLUMN_MUSCLE_GROUP + " TEXT, " +
                        EXERCISES_COLUMN_MNAME + " TEXT)"
        );
        db.execSQL(
                "CREATE TABLE " + SETTINGS_TABLE_NAME +
                        "(" + SETTINGS_COLUMN_ID + " TEXT PRIMARY KEY, " +
                        SETTINGS_COLUMN_VALUE + " TEXT)"
        );
        db.execSQL(
                "CREATE TABLE " + INITWEIGHT_TABLE_NAME+
                        "(" + INITWEIGHT_COLUMN_ID + " TEXT PRIMARY KEY, " +
                        INITWEIGHT_COLUMN_WEIGHT + " REAL)"
        );

    }

    /**
     * Insert deafult Settings.
     * @param SqliteDatabase db
     */
    public void insertSettings() {
        SQLiteDatabase db = this.getWritableDatabase();
        insertSettings("open_keyboard_by_middle_button", "false", db);
    }

    /**
     * Databse Upgrade, is called when the Version is upgraded.
     * @param SqliteDatabase db
     * @param Version int oldVersion
     * @param Version int newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + EXERCISES_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + INITWEIGHT_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + SETTINGS_TABLE_NAME);
        onCreate(db);
    }

    /**
     * Fügt eine neue ausgeführte Exercise hinzu.
     *
     * @param Date    date
     * @param String  exerciseId
     * @param Integer set
     * @param Integer weight
     * @param Integer repeats
     * @return boolean true wenns geklappt hat. :D
     */
    public boolean insertExecutedExercise(Date date, String exerciseId, Integer set, Double weight, Integer repeats) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(EXECUTED_COLUMN_TIME, formatDate(date));
        contentValues.put(EXECUTED_COLUMN_EXERCISE_ID, exerciseId);
        contentValues.put(EXECUTED_COLUMN_SET, set);
        contentValues.put(EXECUTED_COLUMN_WEIGHT, weight);
        contentValues.put(EXECUTED_COLUMN_REPEATS, repeats);

        db.insert(EXECUTED_TABLE_NAME, null, contentValues);
        return true;
    }

    /**
     * Insert new Setting.
     * @param String id
     * @param String value
     * @param SqliteDatabase db
     * @return
     */
    public boolean insertSettings(String id, String value, SQLiteDatabase db) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(SETTINGS_COLUMN_ID, id);
        contentValues.put(SETTINGS_COLUMN_VALUE, value);

        db.insert(SETTINGS_TABLE_NAME, null, contentValues);
        return true;
    }

    /**
     * Update Setting
     * @param String id
     * @param String newVlaue
     * @return true when ok.
     */
    public boolean updateSettings(String id, String newVlaue) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues newValues = new ContentValues();
        newValues.put(SETTINGS_COLUMN_VALUE, newVlaue);
        db.update(SETTINGS_TABLE_NAME, newValues, SETTINGS_COLUMN_ID + " = ?" , new String[]{id});
        return true;
    }

    /**
     * Getts the SettingsValue for a settingsId.
     * @param String settingId
     * @return null -> SettingId not exsit
     */
    public String getSettingsValue(String settingId) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + SETTINGS_TABLE_NAME + " WHERE " +
                SETTINGS_COLUMN_ID + " = ? ", new String[]{settingId});

        if(res.moveToFirst()) {
            return res.getString(1);
        }


        return null;
    }

    /**
     * Inserting new Execirse.
     * @param String id
     * @param String name
     * @param String imageUrl
     * @param String description
     * @param String muscleGroup
     * @param String machineName
     * @return true when ok.
     */
    public boolean insertExercise(String id, String name, String imageUrl, String description, String muscleGroup, String machineName) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(EXERCISES_COLUMN_EXERCISE_ID, id);
        contentValues.put(EXERCISES_COLUMN_ENAME, name);
        contentValues.put(EXERCISES_COLUMN_IMAGE_URL, imageUrl);
        contentValues.put(EXERCISES_COLUMN_DESCRIPTION, description);
        contentValues.put(EXERCISES_COLUMN_MUSCLE_GROUP, muscleGroup);
        contentValues.put(EXERCISES_COLUMN_MNAME, machineName);

        db.insert(EXERCISES_TABLE_NAME, null, contentValues);
        return true;
    }

    /**
     * Gets all Executed Exercises in the Database.
     * @return ArrayList with all Executed Exercises
     */
    public ArrayList<Executed> getAllExecutedExercises() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + EXECUTED_TABLE_NAME + " ORDER BY " + EXECUTED_COLUMN_TIME, null);

        ArrayList<Executed> executeds = new ArrayList<Executed>();

        if (res.moveToFirst()) {
            do {
                Date time = getDateFromString(res.getString(0));
                String id = res.getString(1);
                int set = res.getInt(2);
                Double weight = res.getDouble(3);
                int repeats = res.getInt(4);
                Executed executed = new Executed(time, id, set, weight, repeats);

                executeds.add(executed);
            } while (res.moveToNext());
        }

        return executeds;

    }

    /**
     * Gets all Exercises in the Database.
     * @return ArrayList with all Exercises
     */
    public ArrayList<Exercise> getAllExercises() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + EXERCISES_TABLE_NAME + " ORDER BY " + EXERCISES_COLUMN_EXERCISE_ID, null);

        ArrayList<Exercise> exercises = new ArrayList<Exercise>();

        if (res.moveToFirst()) {
            do {
                String id = res.getString(0);
                String ename = res.getString(1);
                String img = res.getString(2);
                String des = res.getString(3);
                String mg = res.getString(4);
                String mname = res.getString(5);
                Exercise exercise = new Exercise(id, des, ename, img, mg, mname );


                exercises.add(exercise);
            } while (res.moveToNext());
        }

        return exercises;

    }

    /**
     * Gets all Sets from the Day by the execiseId.
     * @param Date today -> Day to Search
     * @param String execiseId -> ID to Search
     * @return ArrayList<Executed>, if no Sets are added for today than ist the ArrayList Empty!
     */
    public ArrayList<Executed> getSetsFromDay(Date today, String execiseId) {
        ArrayList<Executed> setsDay = new ArrayList<>();
        ArrayList<Executed> executs = getSetsFromExecutedExercisesId(execiseId);
        for(Executed exe : executs) {
            if(Util.isSameDay(today, exe.getTime())) {
                setsDay.add(exe);
            }
        }
        return setsDay;
    }

    /**
     * Deletes a Executed Exercise with the Keys Set, Date and execiseId.
     * @param Date today, from which Day it have to deleted
     * @param Integer set
     * @param String execiseId
     * @return true if ok.
     */
    public boolean deleteExecuted(Date today, Integer set, String execiseId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<Executed> executs = getSetsFromExecutedExercisesId(execiseId);
        for(Executed exe : executs) {
            if(Util.isSameDay(today, exe.getTime())) {
                if(exe.getSet() == set) {
                    db.delete(EXECUTED_TABLE_NAME, EXECUTED_COLUMN_EXERCISE_ID + " = ? AND "
                            + EXECUTED_COLUMN_SET + " = ? AND "  + EXECUTED_COLUMN_TIME + " = ?", new String[] {exe.getExerciseId(), exe.getSet() + "", formatDate(exe.getTime())});
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Deletes all Executed Exercises from a specific Day.
     * @param Date today
     * @param String execiseId
     * @return true if ok.
     */
    public boolean deleteAllFromToday(Date today, String execiseId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<Executed> executs = getSetsFromExecutedExercisesId(execiseId);
        for(Executed exe : executs) {
            Log.i("Delete: ", "Exe " + formatDate(exe.getTime()) );
            if(Util.isSameDay(today, exe.getTime())) {
                int i = db.delete(EXECUTED_TABLE_NAME, EXECUTED_COLUMN_EXERCISE_ID + " = ?  AND "
                        + EXECUTED_COLUMN_TIME + " = ?", new String[] {exe.getExerciseId(), formatDate(exe.getTime())});
                    return true;
            }

        }

        return false;
    }

    /**
     * All Sets from one ExeciseId.
     * @param String exerciseId
     * @return ArrayList with all Executed Exercises with the ExerciseId
     */
    public ArrayList<Executed> getSetsFromExecutedExercisesId(String exerciseId) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + EXECUTED_TABLE_NAME + " WHERE " +
                EXECUTED_COLUMN_EXERCISE_ID + " = ? ORDER BY " + EXECUTED_COLUMN_TIME, new String[]{exerciseId});
        ArrayList<Executed> executeds = new ArrayList<Executed>();

        if (res.moveToFirst()) {
            do {
                Date time = getDateFromString(res.getString(0));
                String id = res.getString(1);
                int set = res.getInt(2);
                Double weight = res.getDouble(3);
                int repeats = res.getInt(4);
                Executed executed = new Executed(time, id, set, weight, repeats);

                executeds.add(executed);
            } while (res.moveToNext());
        }

        return executeds;
    }

    /**
     * Gets the Exercise with the exerciseId.
     * @param String exerciseId
     * @return Exercise
     */
    public Exercise getExerciseFromId(String exerciseId) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + EXERCISES_TABLE_NAME + " WHERE " +
                EXECUTED_COLUMN_EXERCISE_ID + " = ?", new String[]{exerciseId});
        if (res.moveToFirst()) {
            String id = res.getString(0);
            String ename = res.getString(1);

            String img = res.getString(2);
            String des = res.getString(3);
            String mg = res.getString(4);
            String mname = res.getString(5);
            Exercise exercise = new Exercise(id, des, ename, img, mg, mname);
            return exercise;
        }
        return null;
    }

    /**
     * Gets/or Sets the init Weight for the Score.
     * @param String exerciseId
     * @param Double currentWeight
     * @return initWeight
     */
    public Double getInitWeight(String exerciseId, Double currentWeight) {
        SQLiteDatabase db = getWritableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + INITWEIGHT_TABLE_NAME + " WHERE " +
                INITWEIGHT_COLUMN_ID + " = ?", new String[]{exerciseId});
        if (res.moveToFirst()) {
            return res.getDouble(1);
        } else {
            ContentValues contentValues = new ContentValues();

            contentValues.put(INITWEIGHT_COLUMN_ID, exerciseId);
            contentValues.put(INITWEIGHT_COLUMN_WEIGHT, currentWeight);
            db.insert(INITWEIGHT_TABLE_NAME, null, contentValues);
            return currentWeight;
        }
    }

    /**
     * Formats the Date to an String to safe it in the Database.
     * @param Date date
     * @return Date as String
     */
    public String formatDate(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
        return sdf.format(date);
    }

    /**
     * Convert a String to a Date, to read a Date from the Database.
     * @param String date
     * @return String to Date.
     */
    public Date getDateFromString(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
        try {
            return sdf.parse(date);

        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return null;
    }

}
