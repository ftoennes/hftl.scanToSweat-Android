package de.hftl.scanToSweat.utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lsbah on 20.12.2017.
 */
public class ViewPagerAdapter extends FragmentPagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();

    /**
     * Default Constructor.
     * @param manager
     */
    public ViewPagerAdapter(FragmentManager manager) {
        super(manager);
    }

    /***
     * Gets Item from List.
     * @param int position
     * @return Fragment
     */
    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    /**
     * Getter for the Size.
     * @return int size
     */
    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    /**
     * Adds a Fragemnt to the Viewpager
     * @param Fragment fragment
     */
    public void addFragment(Fragment fragment) {
        mFragmentList.add(fragment);
    }

}