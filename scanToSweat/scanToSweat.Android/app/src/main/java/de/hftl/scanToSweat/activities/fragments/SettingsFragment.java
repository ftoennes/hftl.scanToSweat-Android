package de.hftl.scanToSweat.activities.fragments;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import de.hftl.scanToSweat.R;
import de.hftl.scanToSweat.activities.TrainingshistoryActivity;
import de.hftl.scanToSweat.database.DatabaseHelper;

/**
 * contains settings
 *
 * @createdBy leoschul on 12.10.2017.
 * @addedBy KoPreuss
 */
public class SettingsFragment extends Fragment {

    /**
     * Construktor.
     * Does nothing
     */
    public SettingsFragment() {

    }
    //Impressum String
    private String impressum = "";
    private boolean impressumExtended;


    /**
     * Creating the View.
     * @param LayoutInflater inflater
     * @param ViewGroup container
     * @param Bundle savedInstanceState
     * @return View Rootview
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);

        Switch sw = (Switch) rootView.findViewById(R.id.switch3);
        DatabaseHelper dh = new DatabaseHelper(getActivity());
        String value = dh.getSettingsValue("open_keyboard_by_middle_button");
        if(value != null) {
            if (value.equals("true")) {
                sw.setChecked(true);
            }
        } else {
            dh.insertSettings();
        }
        sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                DatabaseHelper dh = new DatabaseHelper(getActivity());
                if(isChecked) {
                    dh.updateSettings("open_keyboard_by_middle_button",  "true");
                } else {
                    dh.updateSettings("open_keyboard_by_middle_button",  "false");
                }
            }
        });

        this.impressum = getResources().getString(R.string.settings_impressum);

        Button buttonImpressum = (Button) rootView.findViewById(R.id.button);
        buttonImpressum.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView textImpressum = (TextView) getActivity().findViewById(R.id.impressum);
             Button buttonImpressum = (Button) getActivity().findViewById(R.id.button);
             if(impressumExtended) {
                 textImpressum.setText("");
                 impressumExtended = false;
                 Drawable arrowDown = getActivity().getResources().getDrawable(R.mipmap.ic_keyboard_arrow_down_black_24dp);
                 buttonImpressum.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowDown, null );
             } else {
                 textImpressum.setText(impressum);
                 impressumExtended = true;
                 Drawable arrowUp = getActivity().getResources().getDrawable(R.mipmap.ic_keyboard_arrow_up_black_24dp);
                 buttonImpressum.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowUp, null );
             }
            }
        });

        return rootView;
    }

}
