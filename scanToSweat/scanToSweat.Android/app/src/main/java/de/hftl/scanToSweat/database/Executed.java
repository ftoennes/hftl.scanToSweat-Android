package de.hftl.scanToSweat.database;

import java.util.Date;

/**
 * Created by leoschul on 24.11.2017.
 */

public class Executed {

    private Date time;
    private String exerciseId;
    private int set;
    private Double weight;
    private int repeats;

    /**
     * Default Construtor.
     * @param Date time
     * @param String exerciseId
     * @param int set
     * @param Double weight
     * @param int repeats
     */
    public Executed(Date time, String exerciseId, int set, Double weight, int repeats) {
        this.time = time;
        this.exerciseId = exerciseId;
        this.set = set;
        this.weight = weight;
        this.repeats = repeats;
    }

    /**
     * Getter for Time.
     * @return Date time
     */
    public Date getTime() {
        return time;
    }

    /**
     * Setter for Time.
     * @param Date time
     */
    public void setTime(Date time) {
        this.time = time;
    }

    /**
     * Getter for ExerciseId.
     * @return String ExerciseId
     */
    public String getExerciseId() {
        return exerciseId;
    }

    /**
     * Setter for exerciseId.
     * @param String exerciseId
     */
    public void setExerciseId(String exerciseId) {
        this.exerciseId = exerciseId;
    }

    /**
     * Getter for Set.
     * @return int Set
     */
    public int getSet() {
        return set;
    }

    /**
     * Setter for Set.
     * @param int set
     */
    public void setSet(int set) {
        this.set = set;
    }

    /**
     * Getter for Weight.
     * @return Double Weight
     */
    public Double getWeight() {
        return weight;
    }

    /**
     * Setter for Weight.
     * @param Double weight
     */
    public void setWeight(Double weight) {
        this.weight = weight;
    }

    /**
     * Getter for Repeats.
     * @return int Repeats
     */
    public int getRepeats() {
        return repeats;
    }

    /**
     * Setter for Repeats.
     * @param int repeats
     */
    public void setRepeats(int repeats) {
        this.repeats = repeats;
    }
}
