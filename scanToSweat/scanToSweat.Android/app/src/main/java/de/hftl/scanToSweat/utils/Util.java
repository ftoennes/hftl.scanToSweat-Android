package de.hftl.scanToSweat.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import de.hftl.scanToSweat.database.Executed;

/**
 * Created by lsbah on 20.12.2017.
 */

public class Util {

    /**
     * Checks if Dates are on Same Day.
     * @param Date date1
     * @param Date date2
     * @return sameDay -> true | not sameDay -> false
     */
    public static boolean isSameDay(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(date1);
        cal2.setTime(date2);
        boolean sameDay = cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
        return  sameDay;
    }


    /**
     * filter all Executed exercises to unique node ids
     * @param allExecuted List of all Executed
     * @return filtered list with unique node ids
     */
    public static  ArrayList<String> filterAllExecutedForUniqueNodeId (ArrayList<Executed> allExecuted ){
        ArrayList<String> allExecutedNodeIds = new ArrayList<>();
        allExecutedNodeIds.add(allExecuted.get(0).getExerciseId());
        for (int k = 0; k < allExecuted.size(); k++){
            boolean temp = false;
            for (int m = 0; m< allExecutedNodeIds.size(); m++){
                if(allExecuted.get(k).getExerciseId().equals(allExecutedNodeIds.get(m))){
                    temp = true;
                }
            }
            if (!temp){
                allExecutedNodeIds.add(allExecuted.get(k).getExerciseId());
            }
        }
        return allExecutedNodeIds;
    }

    /**
     * filters all executed for given Date
     * @param allExecuted List of all executed
     * @param neededDate date to which should filtered
     * @return filtered list for neededDate
     */
    public  static ArrayList<Executed> filterChildListForGivenDate(ArrayList<Executed> allExecuted, Date neededDate){
        ArrayList <Executed> childListForDate = new ArrayList();
        for(int index = 0; index< allExecuted.size(); index++){
            if(isSameDay(allExecuted.get(index).getTime(), neededDate)){
                childListForDate.add(allExecuted.get(index));
            }
        } //helplist contains now all executed for given date
        return childListForDate;
    }

    /**
     * Filters the list of given childelements which is allready filtered by a given date after a specific nodeID
     * @param childListForDate List with all entries for one date
     * @param nodeId param of which should be filteres
     * @return filtered list for given nodeId
     */
    public static ArrayList filterChildListForGivenExerciseID(ArrayList<Executed> childListForDate, String nodeId){
        ArrayList childListForNode = new ArrayList();
        for(int i = 0; i < childListForDate.size(); i++){
            if(childListForDate.get(i).getExerciseId().equals(nodeId)){
                childListForNode.add(childListForDate.get(i));
            }
        }
        return childListForNode;
    }

    /**
     * filters for the Executed by identical sets on the same day which has the highest set
     * @param helperListForHighestSet list of which should be filterd the highest number of set
     * @return the Executed with the highest Set number
     */
    public static Executed filterChildForHighestSet(ArrayList<Executed> helperListForHighestSet){
        int maxValue = helperListForHighestSet.get(0).getSet();
        Executed executed = helperListForHighestSet.get(0);
        for (int i = 0; i < helperListForHighestSet.size(); i++){
            if(helperListForHighestSet.get(i).getSet() > maxValue){
                maxValue = helperListForHighestSet.get(i).getSet();
                executed = helperListForHighestSet.get(i);
            }
        }
        return executed;
    }

}
