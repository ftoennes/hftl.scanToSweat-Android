package de.hftl.scanToSweat.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import de.hftl.scanToSweat.R;
import de.hftl.scanToSweat.database.DatabaseHelper;
import de.hftl.scanToSweat.database.Executed;
import de.hftl.scanToSweat.database.Exercise;
import de.hftl.scanToSweat.utils.CustomButtonListener;
import de.hftl.scanToSweat.utils.ExpandableListAdapter;
import de.hftl.scanToSweat.utils.Util;

public class ExerciseDetailActivity extends Activity {

    static final int MIN_DISTANCE = 150;
    String exerciseId;
    String exerciseName;
    ExpandableListAdapter expandableListAdapter;
    ExpandableListView expandableListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    Button button;
    float x1, x2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_detail);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        TextView titel = (TextView) findViewById(R.id.textView14);
        if (!bundle.isEmpty()) {
            exerciseId = (String) bundle.get("ID");
            exerciseName = (String) bundle.get("Name");
            titel.setText(exerciseName);
        } else {

        }


        button = (Button) findViewById(R.id.buttonSpielen);
        button.setText(getResources().getString(R.string.exercisedetail_buttonlabel_openexcercise));
        button.setOnClickListener(new CustomButtonListener(exerciseId, exerciseName) {
            public void onClick(View view) {
                Intent scannedLine = new Intent(view.getContext(), ExerciseActivity.class);
                scannedLine.putExtra("scannedLineFields", getId());
                view.getContext().startActivity(scannedLine);
            }
        });
        expandableListView = (ExpandableListView) findViewById(R.id.lvExp_exercise);


        prepareListData();
        expandableListAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);
        expandableListView.setAdapter(expandableListAdapter);

    }

    /**
     * preparing ListData
     */
    private void prepareListData() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        listDataHeader = new ArrayList<>();

        listDataChild = new HashMap<String, List<String>>();
        DatabaseHelper databaseHelper = new DatabaseHelper(this);

        ArrayList<Executed> allexecuted = databaseHelper.getAllExecutedExercises();
        ArrayList<Exercise> allExercises = databaseHelper.getAllExercises();


        ArrayList allDates = new ArrayList();
        ArrayList<Executed> entriesForOneDate = new ArrayList<>();
        ArrayList<String> allExecutedNodeIds = new ArrayList<>();
        ArrayList<Executed> allSetForOneDateAndOndNode = new ArrayList<>();
        ArrayList detailsForExecuted = new ArrayList();
        Date temp = new Date(2000000000);

        for (int i = 0; i < allexecuted.size(); i++) {
            if (!Util.isSameDay(allexecuted.get(i).getTime(), temp)) {
                if (allexecuted.get(i).getExerciseId().equals(exerciseId)) {
                    allDates.add(allexecuted.get(i).getTime());
                }
            }
            temp = allexecuted.get(i).getTime();
        }

        for (int j = 0; j < allDates.size(); j++) {
            listDataHeader.add(simpleDateFormat.format(allDates.get(j)));
        }

        for (int i = 0; i < allDates.size(); i++) {
            entriesForOneDate = Util.filterChildListForGivenDate(allexecuted, (Date) allDates.get(i));
            allSetForOneDateAndOndNode = Util.filterChildListForGivenExerciseID(entriesForOneDate, exerciseId);
            for (int j = 0; j < allSetForOneDateAndOndNode.size(); j++) {
                detailsForExecuted.add(getResources().getString(R.string.exercisedetail_label_set) + ": " + allSetForOneDateAndOndNode.get(j).getSet() +
                        "\t " + getResources().getString(R.string.exercisedetail_label_repeats) + ": " + String.valueOf(allSetForOneDateAndOndNode.get(j).getRepeats()) +
                        "\t " + getResources().getString(R.string.exercisedetail_label_weight) + ": " + String.valueOf(allSetForOneDateAndOndNode.get(j).getWeight()) + " kg");
                //detailsForExecuted.add("Repeats: " + String.valueOf(allSetForOneDateAndOndNode.get(j).getRepeats()));
                //detailsForExecuted.add("Weight: " + String.valueOf(allSetForOneDateAndOndNode.get(j).getWeight()));
            }
            listDataChild.put(listDataHeader.get(i), detailsForExecuted);
            detailsForExecuted = new ArrayList();

        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        int action = MotionEventCompat.getActionMasked(event);
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                x1 = event.getX();
                break;
            case MotionEvent.ACTION_UP:
                x2 = event.getX();
                float deltaX = x2 - x1;
                if (Math.abs(deltaX) > MIN_DISTANCE) {
                    this.onBackPressed();
                }
                break;
        }
        return super.onTouchEvent(event);
    }
}
